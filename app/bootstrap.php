<?php

require_once __DIR__ . "/../vendor/autoload.php";
$app = require_once __DIR__ . "/app.php";
require_once __DIR__ . "/routes.php";
require_once __DIR__ . "/config.php";

return $app;
