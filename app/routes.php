<?php

/** @var \Silex\Application $app */

use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;

$app->get('/', function () use ($app) {
    return new RedirectResponse($app['routes']->get('auth.index')->getPath());
});

// Auth
$app->mount('/auth', function (ControllerCollection $app) {
    $app->get('/login', 'Cidadania\Application\Controller\LoginController:index')->bind('auth.index');
    $app->get('/logout', 'Cidadania\Application\Controller\LoginController:logout')->bind('auth.logout');
});

$app->mount('/admin', function (ControllerCollection $app) {
    $app->get('/', function () use ($app) {
        return new RedirectResponse($app['routes']->get('contact.index')->getPath());
    });

    $app->post('/authenticate', 'Cidadania\Application\Controller\LoginController:authenticate')->bind('auth.auth');

    // Contact
    $app->mount('/contact', function (ControllerCollection $app) {
        $app->get('/', 'Cidadania\Application\Controller\ContactController:index')->bind('contact.index');
        $app->get('/new', 'Cidadania\Application\Controller\ContactController:new')->bind('contact.new');
        $app->get('/edit/{id}', 'Cidadania\Application\Controller\ContactController:edit')->bind('contact.edit');
        $app->post('/update', 'Cidadania\Application\Controller\ContactController:update')->bind('contact.update');
        $app->post('/create', 'Cidadania\Application\Controller\ContactController:create')->bind('contact.create');
        $app->post('/upload', 'Cidadania\Application\Controller\ContactController:upload')->bind('contact.upload');
        $app->get('/delete', 'Cidadania\Application\Controller\ContactController:delete')->bind('contact.delete');
    });

    // Currency
    $app->mount('/currency', function (ControllerCollection $app) {
        $app->get('/', 'Cidadania\Application\Controller\CurrencyController:index')->bind('currency.index');
        $app->get('/new', 'Cidadania\Application\Controller\CurrencyController:new')->bind('currency.new');
        $app->get('/edit/{id}', 'Cidadania\Application\Controller\CurrencyController:edit')->bind('currency.edit');
        $app->post('/update', 'Cidadania\Application\Controller\CurrencyController:update')->bind('currency.update');
        $app->post('/create', 'Cidadania\Application\Controller\CurrencyController:create')->bind('currency.create');
        $app->get('/delete', 'Cidadania\Application\Controller\CurrencyController:delete')->bind('currency.delete');
    });

    // Partner
    $app->mount('/partner', function (ControllerCollection $app) {
        $app->get('/', 'Cidadania\Application\Controller\PartnerController:index')->bind('partner.index');
        $app->get('/new', 'Cidadania\Application\Controller\PartnerController:new')->bind('partner.new');
        $app->get('/edit/{id}', 'Cidadania\Application\Controller\PartnerController:edit')->bind('partner.edit');
        $app->post('/update', 'Cidadania\Application\Controller\PartnerController:update')->bind('partner.update');
        $app->post('/create', 'Cidadania\Application\Controller\PartnerController:create')->bind('partner.create');
        $app->get('/delete', 'Cidadania\Application\Controller\PartnerController:delete')->bind('partner.delete');
    });

    // Service
    $app->mount('/service', function (ControllerCollection $app) {
        $app->get('/', 'Cidadania\Application\Controller\ServiceController:index')->bind('service.index');
        $app->get('/new', 'Cidadania\Application\Controller\ServiceController:new')->bind('service.new');
        $app->get('/edit/{id}', 'Cidadania\Application\Controller\ServiceController:edit')->bind('service.edit');
        $app->post('/update', 'Cidadania\Application\Controller\ServiceController:update')->bind('service.update');
        $app->post('/create', 'Cidadania\Application\Controller\ServiceController:create')->bind('service.create');
        $app->get('/delete', 'Cidadania\Application\Controller\ServiceController:delete')->bind('service.delete');
    });

    // Assistence
    $app->mount('/assistence', function (ControllerCollection $app) {
        $app->get('/', 'Cidadania\Application\Controller\AssistenceController:index')->bind('assistence.index');
        $app->get('/new', 'Cidadania\Application\Controller\AssistenceController:new')->bind('assistence.new');
        $app->get('/edit/{id}', 'Cidadania\Application\Controller\AssistenceController:edit')->bind('assistence.edit');
        $app->post('/update', 'Cidadania\Application\Controller\AssistenceController:update')->bind('assistence.update');
        $app->post('/create', 'Cidadania\Application\Controller\AssistenceController:create')->bind('assistence.create');
        $app->get('/delete', 'Cidadania\Application\Controller\AssistenceController:delete')->bind('assistence.delete');
    });

    // Payment
    $app->mount('/payment', function (ControllerCollection $app) {
        $app->get('/{id}', 'Cidadania\Application\Controller\PaymentController:index')->bind('payment.contact.index');
        $app->post('/create',
            'Cidadania\Application\Controller\PaymentController:modify')->bind('payment.contact.modify');
        $app->get('/paid/{id}', 'Cidadania\Application\Controller\PaymentController:paid')->bind('payment.paid');
        $app->get('/unpaid/{id}', 'Cidadania\Application\Controller\PaymentController:unpaid')->bind('payment.unpaid');
    });

    // Partner
    $app->mount('/users', function (ControllerCollection $app) {
        $app->get('/', 'Cidadania\Application\Controller\AuthController:index')->bind('user.index');
        $app->get('/new', 'Cidadania\Application\Controller\AuthController:new')->bind('user.new');
        $app->get('/edit/{id}', 'Cidadania\Application\Controller\AuthController:edit')->bind('user.edit');
        $app->post('/update', 'Cidadania\Application\Controller\AuthController:update')->bind('user.update');
        $app->post('/create', 'Cidadania\Application\Controller\AuthController:create')->bind('user.create');
    });
});
