<?php

use Cidadania\Application\ServiceProvider as App;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\TwigServiceProvider;
use WhoopsSilex\WhoopsServiceProvider;

$app = new Silex\Application();

const DATE_MYSQL_FORMAT = 'Y-m-d H:i:s';

$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new WhoopsServiceProvider());
$app->register(new DoctrineServiceProvider());
$app->register(new App\ApplicationServiceProvider());
$app->register(new App\InfrastructureServiceProvider());
$app->register(new App\CommandBusServiceProvider());
$app->register(new TwigServiceProvider(), [
    'twig.path' => __DIR__ . '/../templates',
    'twig.options' => [
        'debug' => true,
    ],
]);
$app->register(new HttpFragmentServiceProvider());
$app->register(new SessionServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new LocaleServiceProvider());
$app->register(new TranslationServiceProvider());
$app->register(new SecurityServiceProvider());

return $app;
