<?php

// application configuration
use Cidadania\Application\Form\ContactForm;
use Cidadania\Application\Form\ContactUploadForm;
use Cidadania\Application\Form\CurrencyForm;
use Cidadania\Application\Form\PartnerForm;
use Cidadania\Application\Form\PaymentForm;
use Cidadania\Application\Form\ServiceForm;
use Cidadania\Application\ServiceProvider\UserProvider;
use Cidadania\Domain\Listener\PaymentListener;
use Cidadania\Domain\Repository\Read\AuthRepositoryInterface;

$app['name'] = 'cidadania';
$app['debug'] = true;
$app['app.log_root_dir'] = sprintf('/var/log/%s', $app['name']);

// db config
$app['dbs.options'] = [
    'default' => [
        'dbname' => getenv('DATABASE_DBNAME'),
        'user' => getenv('DATABASE_USER'),
        'password' => getenv('DATABASE_PASSWORD'),
        'host' => getenv('DATABASE_HOST'),
        'driver' => getenv('DATABASE_DRIVER'),
    ],
];

$app['upload.path'] = __DIR__ . '/../public/upload';

// Dispatcher
$app['dispatcher']->addSubscriber($app[PaymentListener::class]);

$app['security.firewalls'] = [
    'admin' => [
        'pattern' => '^/admin',
        'form' => [
            'login_path' => '/auth/login',
            'check_path' => '/admin/authenticate',
            'default_target_path' => '/admin/contact',
        ],
        'logout' => [
            'logout_path' => '/auth/logout',
            'invalidate_session' => true,
        ],
        'users' => function () use ($app) {
            return new UserProvider($app[AuthRepositoryInterface::class]);
        },
    ],
];

$app->extend('form.types', function ($types) use ($app) {
    $types[] = ContactForm::class;
    $types[] = ContactUploadForm::class;
    $types[] = CurrencyForm::class;
    $types[] = PartnerForm::class;
    $types[] = PaymentForm::class;
    $types[] = ServiceForm::class;

    return $types;
});
