<?php

namespace Cidadania\Infrastructure\Traits;

use Collections\Vector;
use Doctrine\DBAL\Driver\Statement;

trait VectorRepository
{
    /**
     * @param Statement $statement
     * @param string $model
     *
     * @param callable $callable
     *
     * @return mixed
     * @internal param array $data
     */
    public function generateSingleRecordFromQueryResult(Statement $statement, string $model, callable $callable = null)
    {
        $data = $statement->fetch();

        if (empty($data)) {
            return new $model();
        }

        if (!empty($callable)) {
            $data = $callable($data);
        }

        return $model::create($data);
    }

    /**
     * @param Statement $statement
     * @param string $model
     * @param callable|null $callable
     *
     * @return array
     */
    public function generateVectorFromQueryResult(Statement $statement, string $model, callable $callable = null): array
    {
        $data = $statement->fetchAll();
        if (empty($data)) {
            return [];
        }

        $vector = new Vector();
        foreach ($data as $key => $item) {
            if ($callable !== null) {
                $item = $callable($item);
            }

            $vector->add(
                $model::create($item)
            );
        }

        return $vector->toArray();
    }
}
