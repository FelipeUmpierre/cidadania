<?php

namespace Cidadania\Infrastructure\Traits;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

trait FormField
{
    public function addHiddenIdField($data, FormBuilderInterface$form)
    {
        if (!empty($data->getId())) {
            $form->add('id', HiddenType::class);
        }
    }
}