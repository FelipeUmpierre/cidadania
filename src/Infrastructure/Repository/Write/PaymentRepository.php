<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Business\Command\Payment\WritePaymentCommand;
use Cidadania\Domain\Model\Entity;
use Cidadania\Domain\Model\Payment;
use Cidadania\Domain\Repository\Write\PaymentRepositoryInterface;
use Cidadania\Infrastructure\Repository\BaseRepository;

class PaymentRepository extends WriteRepositoryAbstract implements PaymentRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTableName(): string
    {
        return 'payment';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Payment::ID;
    }
}
