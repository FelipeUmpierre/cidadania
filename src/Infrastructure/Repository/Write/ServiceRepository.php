<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Domain\Model\Service;
use Cidadania\Domain\Repository\Write\ServiceRepositoryInterface;

class ServiceRepository extends WriteRepositoryAbstract implements ServiceRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTableName(): string
    {
        return 'service';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Service::ID;
    }
}
