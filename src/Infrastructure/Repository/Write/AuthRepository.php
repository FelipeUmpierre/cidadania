<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Business\Command\Auth\WriteAuthCommand;
use Cidadania\Domain\Model\Entity;
use Cidadania\Domain\Repository\Write\AuthRepositoryInterface;
use Cidadania\Infrastructure\Repository\BaseRepository;

class AuthRepository extends BaseRepository implements AuthRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function insert(WriteAuthCommand $command): int
    {
        return $this->connection->insert(
            $this->getTableName(),
            $command->jsonSerialize()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function update(WriteAuthCommand $command): int
    {
        $serialize = $command->jsonSerialize();

        return $this->connection->update($this->getTableName(), $serialize, [
            'id_contact' => $command->id,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(WriteAuthCommand $command): int
    {
        return $this->connection->update($this->getTableName(), [
            'deleted_at' => (new \DateTime())->format(DATE_MYSQL_FORMAT),
        ], [
            'id_contact' => $command->id,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getTableName(): string
    {
        return 'auth';
    }

    /**
     * @param Entity $entity
     *
     * @return int
     */
    public function save(Entity $entity): int
    {
        // TODO: Implement save() method.
    }
}
