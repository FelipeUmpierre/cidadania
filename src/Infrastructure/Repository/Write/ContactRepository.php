<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Business\Command\Contact\WriteContactCommand;
use Cidadania\Domain\Model\Contact;
use Cidadania\Domain\Model\Entity;
use Cidadania\Domain\Repository\Write\ContactRepositoryInterface;
use Cidadania\Infrastructure\Repository\BaseRepository;

class ContactRepository extends WriteRepositoryAbstract implements ContactRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTableName(): string
    {
        return 'contact';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Contact::ID;
    }
}
