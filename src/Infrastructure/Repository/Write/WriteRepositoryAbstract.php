<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Domain\Model\Entity;
use Cidadania\Domain\Repository\Write\WriteRepositoryInterface;
use Cidadania\Infrastructure\Repository\BaseRepository;

abstract class WriteRepositoryAbstract extends BaseRepository implements WriteRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function delete($id): int
    {
        if (is_array($id)) {
            return $this->connection->delete(
                $this->getTableName(),
                $id
            );
        }

        return $this->connection->update(
            $this->getTableName(),
            [
                'deleted_at' => (new \DateTime)->format(DATE_MYSQL_FORMAT),
            ],
            [
                $this->getIdentifier() => $id,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function save(Entity $entity): int
    {
        $payload = array_filter($entity->toArray(), function ($element) {
            return $element !== null;
        });

        // map through the elements from the payload to change the boolean value to int
        $payload = array_map(function($element) {
            if (is_bool($element)) {
                return (int)$element;
            }

            return $element;
        }, $payload);

        $now = (new \DateTime)->format(DATE_MYSQL_FORMAT);
        
        if ($entity->getId() === null) {
            $payload = array_merge($payload, [
                'created_at' => $now,
            ]);

            $this->connection->insert(
                $this->getTableName(),
                $payload
            );

            return $this->connection->lastInsertId();
        }

        $payload = array_merge($payload, [
            'updated_at' => $now,
        ]);

        $this->connection->update(
            $this->getTableName(),
            $payload, [
                $entity->getIdentifierName() => $entity->getId(),
            ]
        );

        return $entity->getId();
    }

    /**
     * @return string
     */
    abstract protected function getTableName(): string;

    /**
     * @return string
     */
    abstract protected function getIdentifier(): string;
}
