<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Business\Command\PaymentParcels\WritePaymentParcelsCommand;
use Cidadania\Domain\Model\Entity;
use Cidadania\Domain\Model\PaymentParcels;
use Cidadania\Domain\Repository\Write\PaymentParcelRepositoryInterface;
use Cidadania\Infrastructure\Repository\BaseRepository;

class PaymentParcelRepository extends WriteRepositoryAbstract implements PaymentParcelRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTableName(): string
    {
        return 'payment_parcel';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return PaymentParcels::ID;
    }
}
