<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Business\Command\Partner\WritePartnerCommand;
use Cidadania\Domain\Model\Entity;
use Cidadania\Domain\Model\Partner;
use Cidadania\Domain\Repository\Write\PartnerRepositoryInterface;
use Cidadania\Infrastructure\Repository\BaseRepository;

class PartnerRepository extends WriteRepositoryAbstract implements PartnerRepositoryInterface
{
    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'partner';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Partner::ID;
    }
}
