<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Business\Command\Currency\WriteCurrencyCommand;
use Cidadania\Domain\Model\Currency;
use Cidadania\Domain\Model\Entity;
use Cidadania\Domain\Repository\Write\CurrencyRepositoryInterface;
use Cidadania\Infrastructure\Repository\BaseRepository;

class CurrencyRepository extends WriteRepositoryAbstract implements CurrencyRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTableName(): string
    {
        return 'currency';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Currency::ID;
    }
}
