<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Domain\Model\Assistence;
use Cidadania\Domain\Repository\Write\AssistenceRepositoryInterface;

class AssistenceRepository extends WriteRepositoryAbstract implements AssistenceRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTableName(): string
    {
        return 'assistence';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Assistence::ID;
    }
}
