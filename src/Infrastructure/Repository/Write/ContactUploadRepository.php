<?php

namespace Cidadania\Infrastructure\Repository\Write;

use Cidadania\Domain\Model\ContactUpload;
use Cidadania\Domain\Repository\Write\ContactUploadRepositoryInterface;

class ContactUploadRepository extends WriteRepositoryAbstract implements ContactUploadRepositoryInterface
{
    /**
     * @return string
     */
    public function getTableName(): string
    {
        return 'contact_upload';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return ContactUpload::ID;
    }
}
