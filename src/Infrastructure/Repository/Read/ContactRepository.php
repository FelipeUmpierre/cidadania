<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\Contact;
use Cidadania\Domain\Repository\Read\ContactRepositoryInterface;

class ContactRepository extends ReadRepositoryAbstract implements ContactRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClassName(): string
    {
        return Contact::class;
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'contact';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Contact::ID;
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findBy(array $criteria = []): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select('c.*', 'a.name AS fk_assistence')
            ->from($this->getTableName(), 'c')
            ->join('c', 'assistence', 'a', 'a.id = c.fk_assistence')
            ->where('c.deleted_at IS NULL');

        foreach ($criteria as $key => $criterion) {
            $query->andWhere("$key = :$key")->setParameter($key, $criterion);
        }

        return $this->generateVectorFromQueryResult(
            $query->execute(),
            $this->getClassName()
        );
    }
}
