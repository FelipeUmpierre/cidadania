<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\PaymentParcels;
use Cidadania\Domain\Repository\Read\PaymentParcelRepositoryInterface;

class PaymentParcelRepository extends ReadRepositoryAbstract implements PaymentParcelRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClassName(): string
    {
        return PaymentParcels::class;
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'payment_parcel';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return PaymentParcels::ID;
    }
}
