<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\Assistence;
use Cidadania\Domain\Repository\Read\AssistenceRepositoryInterface;

class AssistenceRepository extends ReadRepositoryAbstract implements AssistenceRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClassName(): string
    {
        return Assistence::class;
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'assistence';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Assistence::ID;
    }
}
