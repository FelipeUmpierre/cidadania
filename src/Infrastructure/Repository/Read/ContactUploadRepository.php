<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\ContactUpload;
use Cidadania\Domain\Repository\Read\ContactUploadRepositoryInterface;

class ContactUploadRepository extends ReadRepositoryAbstract implements ContactUploadRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClassName(): string
    {
        return ContactUpload::class;
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'contact_upload';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return ContactUpload::ID;
    }
}
