<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\Auth;
use Cidadania\Domain\Repository\Read\AuthRepositoryInterface;

class AuthRepository extends ReadRepositoryAbstract implements AuthRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClassName(): string
    {
        return Auth::class;
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'auth';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Auth::ID;
    }
}
