<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\Partner;
use Cidadania\Domain\Repository\Read\PartnerRepositoryInterface;

class PartnerRepository extends ReadRepositoryAbstract implements PartnerRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClassName(): string
    {
        return Partner::class;
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'partner';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Partner::ID;
    }
}
