<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\Payment;
use Cidadania\Domain\Repository\Read\PaymentRepositoryInterface;

class PaymentRepository extends ReadRepositoryAbstract implements PaymentRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClassName(): string
    {
        return Payment::class;
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'payment';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Payment::ID;
    }
}
