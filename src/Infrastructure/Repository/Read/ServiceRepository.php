<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\Service;
use Cidadania\Domain\Repository\Read\ServiceRepositoryInterface;

class ServiceRepository extends ReadRepositoryAbstract implements ServiceRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClassName(): string
    {
        return Service::class;
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'service';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Service::ID;
    }
}
