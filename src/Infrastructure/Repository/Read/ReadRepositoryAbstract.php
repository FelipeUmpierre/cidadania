<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\Entity;
use Cidadania\Domain\Repository\Read\ReadRepositoryInterface;
use Cidadania\Infrastructure\Repository\BaseRepository;
use Cidadania\Infrastructure\Traits\VectorRepository;

abstract class ReadRepositoryAbstract extends BaseRepository implements ReadRepositoryInterface
{
    use VectorRepository;

    /**
     * @param int $id
     *
     * @return Entity
     */
    public function find(int $id): Entity
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from($this->getTableName())
            ->where(sprintf('%s = ?', $this->getIdentifier()))
            ->andWhere('deleted_at IS NULL')
            ->setParameter(0, $id);

        return $this->generateSingleRecordFromQueryResult(
            $query->execute(),
            $this->getClassName()
        );
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findBy(array $criteria = []): array
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from($this->getTableName())
            ->where('deleted_at IS NULL');

        foreach ($criteria as $key => $criterion) {
            $query->andWhere("$key = :$key")->setParameter($key, $criterion);
        }

        return $this->generateVectorFromQueryResult(
            $query->execute(),
            $this->getClassName()
        );
    }

    /**
     * @return string
     */
    abstract protected function getClassName(): string;

    /**
     * @return string
     */
    abstract protected function getTableName(): string;

    /**
     * @return string
     */
    abstract protected function getIdentifier(): string;
}
