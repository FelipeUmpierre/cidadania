<?php

namespace Cidadania\Infrastructure\Repository\Read;

use Cidadania\Domain\Model\Currency;
use Cidadania\Domain\Repository\Read\CurrencyRepositoryInterface;

class CurrencyRepository extends ReadRepositoryAbstract implements CurrencyRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClassName(): string
    {
        return Currency::class;
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'currency';
    }

    /**
     * @return string
     */
    protected function getIdentifier(): string
    {
        return Currency::ID;
    }
}
