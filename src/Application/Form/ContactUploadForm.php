<?php

namespace Cidadania\Application\Form;

use Cidadania\Domain\Model\ContactUpload;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactUploadForm extends FormAbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('fk_contact', HiddenType::class)
            ->add('file', FileType::class, [
                'multiple' => true,
                'required' => false,
            ])
            ->add('submit', SubmitType::class);
    }

    protected function getEntityClass(): string
    {
        return ContactUpload::class;
    }
}
