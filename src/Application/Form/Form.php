<?php

namespace Cidadania\Application\Form;

use Cidadania\Application\Contract;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class Form implements Contract\Form
{
    /**
     * @var FormFactory
     */
    private $factory;

    /**
     * @var string
     */
    private $type;

    /**
     * @param FormFactory $factory
     * @param string $type
     */
    public function __construct(FormFactory $factory, string $type)
    {
        $this->factory = $factory;
        $this->type = $type;
    }

    /**
     * @param array $options
     *
     * @return FormInterface
     */
    public function createForm(array $options = []): FormInterface
    {
        return $this->factory->create($this->type, null, $options);
    }

    /**
     * @param array $payload
     * @param mixed|null $default
     * @param array $options
     *
     * @return mixed
     *
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \InvalidArgumentException
     */
    public function submit(array $payload, $default = null, array $options = [])
    {
        $form = $this->factory->create($this->type, $default, $options);
        $form->submit($payload);

        $this->assertValid($form);

        return $form->getData();
    }

    /**
     * @param FormInterface $form
     *
     * @throws \InvalidArgumentException
     */
    private function assertValid(FormInterface $form): void
    {
        if ($form->isValid()) {
            return;
        }

        throw new \InvalidArgumentException($this->collectErrors($form));
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function collectErrors(FormInterface $form): array
    {
        $errors = [];

        foreach ($form->getErrors(true) as $error) {
            $name = $error->getOrigin()->getName();

            $errors[$name][] = $error->getMessage();
        }

        return $errors;
    }
}
