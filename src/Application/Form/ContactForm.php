<?php

namespace Cidadania\Application\Form;

use Cidadania\Domain\Model\Contact;
use Cidadania\Domain\Model\Partner;
use Cidadania\Domain\Repository\Read\PartnerRepositoryInterface;
use Cidadania\Domain\Repository\Read\AssistenceRepositoryInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactForm extends FormAbstractType
{
    /**
     * @var PartnerRepositoryInterface
     */
    protected $partnerRepository;

    /**
     * @var AssistenceRepositoryInterface
     */
    protected $assistenceRepository;

    /**
     * ContactForm constructor.
     *
     * @param PartnerRepositoryInterface $partnerRepository
     * @param AssistenceRepositoryInterface $assistenceRepository
     */
    public function __construct(PartnerRepositoryInterface $partnerRepository, AssistenceRepositoryInterface $assistenceRepository)
    {
        $this->partnerRepository = $partnerRepository;
        $this->assistenceRepository = $assistenceRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('partner', ChoiceType::class, [
                'placeholder' => 'Select a partner',
                'choices' => $this->getPartnerData(),
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('assistence', ChoiceType::class, [
                'placeholder' => 'Select an assistence',
                'choices' => $this->getAssistenceData(),
                'attr' => [
                    'class' => 'form-control',
                ],
                'required' => false,
            ])
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('email', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('phone', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('birthday', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('address', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('passport', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('schooling', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('father_name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('mother_name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('profession', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('observation', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'rows' => 5,
                ],
            ])
            ->add('city', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'Place of birth',
            ])
            ->add('contacted_at', TextType::class, [
                'attr' => [
                    'class' => 'form-control form-control-inline date-picker',
                ],
                'required' => false,
            ])
            ->add('is_client', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('is_done', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('submit', SubmitType::class);
    }

    private function getPartnerData(): array
    {
        $partners = [];

        /** @var Partner $partner */
        foreach ($this->partnerRepository->findBy() as $partner) {
            $partners[$partner->getName()] = $partner->getId();
        }

        return $partners;
    }

    private function getAssistenceData(): array
    {
        $assistences = [];

        /** @var Assistence $assistence */
        foreach ($this->assistenceRepository->findBy() as $assistence) {
            $assistences[$assistence->getName()] = $assistence->getId();
        }

        return $assistences;
    }

    protected function getEntityClass(): string
    {
        return Contact::class;
    }
}
