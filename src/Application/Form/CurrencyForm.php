<?php

namespace Cidadania\Application\Form;

use Cidadania\Domain\Model\Currency;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CurrencyForm extends FormAbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('name', TextType::class, [
                'attr' => [
                    'placeholder' => 'e.g. British Pounds',
                    'class' => 'form-control',
                ],
            ])
            ->add('symbol', TextType::class, [
                'attr' => [
                    'placeholder' => 'e.g. GBP',
                    'class' => 'form-control',
                ],
            ])
            ->add('submit', SubmitType::class);
    }

    protected function getEntityClass(): string
    {
        return Currency::class;
    }
}
