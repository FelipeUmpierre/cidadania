<?php

namespace Cidadania\Application\Form;

use Cidadania\Domain\Model\PaymentParcels;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;

class PaymentParcelsForm extends FormAbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('payment', HiddenType::class)
            ->add('parcel_price', TextType::class)
            ->add('is_paid', CheckboxType::class)
            ->add('payment_date', TextType::class);
    }

    protected function getEntityClass(): string
    {
        return PaymentParcels::class;
    }
}
