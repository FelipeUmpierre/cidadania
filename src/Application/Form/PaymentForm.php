<?php

namespace Cidadania\Application\Form;

use Cidadania\Domain\Model\Currency;
use Cidadania\Domain\Model\Payment;
use Cidadania\Domain\Repository\Read\CurrencyRepositoryInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PaymentForm extends FormAbstractType
{
    /**
     * @var CurrencyRepositoryInterface
     */
    protected $currencyRepository;

    /**
     * ContactForm constructor.
     *
     * @param CurrencyRepositoryInterface $currencyRepository
     */
    public function __construct(CurrencyRepositoryInterface $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('contact', HiddenType::class)
            ->add('currency', ChoiceType::class, [
                'placeholder' => 'Select a currency',
                'choices' => $this->getCurrencyData(),
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('parcels', NumberType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('price', MoneyType::class, [
                'currency' => '',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('partner_price', MoneyType::class, [
                'currency' => '',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('total_price', MoneyType::class, [
                'currency' => '',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('start_payment', TextType::class, [
                'attr' => [
                    'class' => 'form-control form-control-inline input-medium date-picker',
                ],
            ])
            ->add('submit', SubmitType::class);
    }

    private function getCurrencyData(): array
    {
        $currencies = [];

        /** @var Currency $currency */
        foreach ($this->currencyRepository->findBy() as $currency) {
            $currencies[$currency->getName()] = $currency->getId();
        }

        return $currencies;
    }

    protected function getEntityClass(): string
    {
        return Payment::class;
    }
}
