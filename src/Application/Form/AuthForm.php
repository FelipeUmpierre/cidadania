<?php

namespace Cidadania\Application\Form;

use Cidadania\Domain\Model\Partner;
use Cidadania\Domain\Repository\Read\PartnerRepositoryInterface;
use Cidadania\Infrastructure\Traits\FormField;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;

class AuthForm
{
    use FormField;

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * ContactForm constructor.
     *
     * @param FormFactory $formFactory
     */
    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param $data
     * @param string|null $action
     *
     * @return FormInterface
     */
    public function buildForm($data, string $action = null): FormInterface
    {
        $form = $this->formFactory->createBuilder(FormType::class, $data)
            ->setMethod('POST')
            ->add('username', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('password', PasswordType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save',
                'attr' => [
                    'class' => 'btn green'
                ]
            ]);

        $this->addHiddenIdField($data, $form);

        if ($action !== null) {
            $form->setAction($action);
        }

        return $form->getForm();
    }
}