<?php

namespace Cidadania\Application\Form;

use Cidadania\Domain\Model\Partner;
use Cidadania\Domain\Model\Service;
use Cidadania\Domain\Repository\Read\ServiceRepositoryInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PartnerForm extends FormAbstractType
{
    /**
     * @var ServiceRepositoryInterface
     */
    protected $serviceRepository;

    /**
     * ContactForm constructor.
     *
     * @param ServiceRepositoryInterface $serviceRepository
     */
    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('service', ChoiceType::class, [
                'placeholder' => 'Select a service',
                'choices' => $this->getServiceData(),
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('email', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('submit', SubmitType::class);
    }

    /**
     * @inheritdoc
     */
    protected function getEntityClass(): string
    {
        return Partner::class;
    }

    /**
     * @return array
     */
    private function getServiceData(): array
    {
        $services = [];
        /** @var Service $service */
        foreach ($this->serviceRepository->findBy() as $service) {
            $services[$service->getName()] = $service->getId();
        }

        return $services;
    }
}
