<?php

namespace Cidadania\Application\Form;

use Cidadania\Domain\Model\Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class FormAbstractType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data' => $this->createEntity(),
            'data_class' => $this->getEntityClass(),
            'method' => 'POST',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function createEntity(): Entity
    {
        $class = $this->getEntityClass();

        return new $class();
    }

    abstract protected function getEntityClass(): string;
}
