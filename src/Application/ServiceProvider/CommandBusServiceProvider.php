<?php

namespace Cidadania\Application\ServiceProvider;

use Bezdomni\Tactician\Pimple\PimpleLocator;
use Cidadania\Business\Command;
use Cidadania\Business\Handler;
use Cidadania\Domain\Repository\Read as DomainRead;
use Cidadania\Domain\Repository\Write as DomainWrite;
use Cidadania\Application\Form;
use League\Tactician\CommandBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\Plugins\LockingMiddleware;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CommandBusServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $container)
    {
        $this->registerCommandBusMiddleware($container);
        $this->registerCommandBus($container);
        $this->registerLocator($container);
        $this->registerContactHandlers($container);
        $this->registerPartnerHandlers($container);
        $this->registerServiceHandlers($container);
        $this->registerAssistenceHandlers($container);
        $this->registerContactUploadHandlers($container);
        $this->registerPaymentHandlers($container);
        $this->registerCurrencyHandler($container);
        $this->registerAuthHandler($container);
        $this->registerPaymentParcelsHandlers($container);
    }

    /**
     * @param Container $container
     */
    private function registerCommandBusMiddleware(Container $container)
    {
        $container['bus.middleware'] = function (Container $container) {
            return [
                new LockingMiddleware(),
                new CommandHandlerMiddleware(
                    new ClassNameExtractor(),
                    $container['bus.locator'],
                    new HandleInflector()
                ),
            ];
        };
    }

    /**
     * Register the command bus
     *
     * @param Container $container
     */
    private function registerCommandBus(Container $container)
    {
        $container['bus'] = function (Container $container) {
            return new CommandBus($container['bus.middleware']);
        };
    }
    /**
     * @param Container $container
     */
    private function registerLocator(Container $container)
    {
        $container['bus.locator'] = function (Container $container) {
            return new PimpleLocator(
                $container,
                [
                    Command\Auth\WriteAuthCommand::class => Handler\Auth\WriteAuthHandler::class,
                    Command\Auth\ListAuthCommand::class => Handler\Auth\ListAuthHandler::class,

                    // Currency
                    Command\Currency\ShowCommand::class => Handler\Currency\ShowHandler::class,
                    Command\Currency\ListCommand::class => Handler\Currency\ListHandler::class,
                    Command\Currency\CreateCommand::class => Handler\Currency\CreateHandler::class,
                    Command\Currency\UpdateCommand::class => Handler\Currency\UpdateHandler::class,
                    Command\Currency\DeleteCommand::class => Handler\Currency\DeleteHandler::class,

                    // Contact
                    Command\Contact\ShowCommand::class => Handler\Contact\ShowHandler::class,
                    Command\Contact\ListCommand::class => Handler\Contact\ListHandler::class,
                    Command\Contact\CreateCommand::class => Handler\Contact\CreateHandler::class,
                    Command\Contact\UpdateCommand::class => Handler\Contact\UpdateHandler::class,
                    Command\Contact\DeleteCommand::class => Handler\Contact\DeleteHandler::class,

                    // ContactUpload
                    Command\ContactUpload\ShowCommand::class => Handler\ContactUpload\ShowHandler::class,
                    Command\ContactUpload\ListCommand::class => Handler\ContactUpload\ListHandler::class,
                    Command\ContactUpload\CreateCommand::class => Handler\ContactUpload\CreateHandler::class,
                    Command\ContactUpload\UpdateCommand::class => Handler\ContactUpload\UpdateHandler::class,
                    Command\ContactUpload\DeleteCommand::class => Handler\ContactUpload\DeleteHandler::class,

                    // Partner
                    Command\Partner\ShowCommand::class => Handler\Partner\ShowHandler::class,
                    Command\Partner\ListCommand::class => Handler\Partner\ListHandler::class,
                    Command\Partner\CreateCommand::class => Handler\Partner\CreateHandler::class,
                    Command\Partner\UpdateCommand::class => Handler\Partner\UpdateHandler::class,
                    Command\Partner\DeleteCommand::class => Handler\Partner\DeleteHandler::class,

                    // Service
                    Command\Service\ShowCommand::class => Handler\Service\ShowHandler::class,
                    Command\Service\ListCommand::class => Handler\Service\ListHandler::class,
                    Command\Service\CreateCommand::class => Handler\Service\CreateHandler::class,
                    Command\Service\UpdateCommand::class => Handler\Service\UpdateHandler::class,
                    Command\Service\DeleteCommand::class => Handler\Service\DeleteHandler::class,

                    // Assistence
                    Command\Assistence\ShowCommand::class => Handler\Assistence\ShowHandler::class,
                    Command\Assistence\ListCommand::class => Handler\Assistence\ListHandler::class,
                    Command\Assistence\CreateCommand::class => Handler\Assistence\CreateHandler::class,
                    Command\Assistence\UpdateCommand::class => Handler\Assistence\UpdateHandler::class,
                    Command\Assistence\DeleteCommand::class => Handler\Assistence\DeleteHandler::class,

                    // Payment
                    Command\Payment\ShowCommand::class => Handler\Payment\ShowHandler::class,
                    Command\Payment\ListCommand::class => Handler\Payment\ListHandler::class,
                    Command\Payment\CreateCommand::class => Handler\Payment\CreateHandler::class,
                    Command\Payment\UpdateCommand::class => Handler\Payment\UpdateHandler::class,
                    Command\Payment\DeleteCommand::class => Handler\Payment\DeleteHandler::class,

                    // PaymentParcels
                    Command\PaymentParcels\ShowCommand::class => Handler\PaymentParcels\ShowHandler::class,
                    Command\PaymentParcels\ListCommand::class => Handler\PaymentParcels\ListHandler::class,
                    Command\PaymentParcels\CreateCommand::class => Handler\PaymentParcels\CreateHandler::class,
                    Command\PaymentParcels\UpdateCommand::class => Handler\PaymentParcels\UpdateHandler::class,
                    Command\PaymentParcels\DeleteCommand::class => Handler\PaymentParcels\DeleteHandler::class,
                ]
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerAuthHandler(Container $container)
    {
        $container[Handler\Auth\WriteAuthHandler::class] = function (Container $container) {
            return new Handler\Auth\WriteAuthHandler(
                $container[DomainWrite\AuthRepositoryInterface::class]
            );
        };

        $container[Handler\Auth\ListAuthHandler::class] = function (Container $container) {
            return new Handler\Auth\ListAuthHandler(
                $container[DomainRead\AuthRepositoryInterface::class]
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerContactHandlers(Container $container)
    {
        $container[Handler\Contact\ListHandler::class] = function (Container $container) {
            return new Handler\Entity\ListEntityHandler(
                $container[DomainRead\ContactRepositoryInterface::class]
            );
        };

        $container[Handler\Contact\ShowHandler::class] = function (Container $container) {
            return new Handler\Entity\ShowEntityHandler(
                $container[DomainRead\ContactRepositoryInterface::class]
            );
        };

        $container[Handler\Contact\DeleteHandler::class] = function (Container $container) {
            return new Handler\Entity\DeleteEntityHandler(
                $container[DomainWrite\ContactRepositoryInterface::class]
            );
        };

        $container[Handler\Contact\CreateHandler::class] = function (Container $container) {
            return new Handler\Entity\CreateEntityHandler(
                $container[DomainWrite\ContactRepositoryInterface::class],
                $container['contact.form']
            );
        };

        $container[Handler\Contact\UpdateHandler::class] = function (Container $container) {
            return new Handler\Entity\UpdateEntityHandler(
                $container[DomainWrite\ContactRepositoryInterface::class],
                $container['contact.form']
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerPartnerHandlers(Container $container)
    {
        $container[Handler\Partner\ListHandler::class] = function (Container $container) {
            return new Handler\Entity\ListEntityHandler(
                $container[DomainRead\PartnerRepositoryInterface::class]
            );
        };

        $container[Handler\Partner\ShowHandler::class] = function (Container $container) {
            return new Handler\Entity\ShowEntityHandler(
                $container[DomainRead\PartnerRepositoryInterface::class]
            );
        };

        $container[Handler\Partner\DeleteHandler::class] = function (Container $container) {
            return new Handler\Entity\DeleteEntityHandler(
                $container[DomainWrite\PartnerRepositoryInterface::class]
            );
        };

        $container[Handler\Partner\CreateHandler::class] = function (Container $container) {
            return new Handler\Entity\CreateEntityHandler(
                $container[DomainWrite\PartnerRepositoryInterface::class],
                $container['partner.form']
            );
        };

        $container[Handler\Partner\UpdateHandler::class] = function (Container $container) {
            return new Handler\Entity\UpdateEntityHandler(
                $container[DomainWrite\PartnerRepositoryInterface::class],
                $container['partner.form']
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerServiceHandlers(Container $container)
    {
        $container[Handler\Service\ListHandler::class] = function (Container $container) {
            return new Handler\Entity\ListEntityHandler(
                $container[DomainRead\ServiceRepositoryInterface::class]
            );
        };

        $container[Handler\Service\ShowHandler::class] = function (Container $container) {
            return new Handler\Entity\ShowEntityHandler(
                $container[DomainRead\ServiceRepositoryInterface::class]
            );
        };

        $container[Handler\Service\DeleteHandler::class] = function (Container $container) {
            return new Handler\Entity\DeleteEntityHandler(
                $container[DomainWrite\ServiceRepositoryInterface::class]
            );
        };

        $container[Handler\Service\CreateHandler::class] = function (Container $container) {
            return new Handler\Entity\CreateEntityHandler(
                $container[DomainWrite\ServiceRepositoryInterface::class],
                $container['service.form']
            );
        };

        $container[Handler\Service\UpdateHandler::class] = function (Container $container) {
            return new Handler\Entity\UpdateEntityHandler(
                $container[DomainWrite\ServiceRepositoryInterface::class],
                $container['service.form']
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerAssistenceHandlers(Container $container)
    {
        $container[Handler\Assistence\ListHandler::class] = function (Container $container) {
            return new Handler\Entity\ListEntityHandler(
                $container[DomainRead\AssistenceRepositoryInterface::class]
            );
        };

        $container[Handler\Assistence\ShowHandler::class] = function (Container $container) {
            return new Handler\Entity\ShowEntityHandler(
                $container[DomainRead\AssistenceRepositoryInterface::class]
            );
        };

        $container[Handler\Assistence\DeleteHandler::class] = function (Container $container) {
            return new Handler\Entity\DeleteEntityHandler(
                $container[DomainWrite\AssistenceRepositoryInterface::class]
            );
        };

        $container[Handler\Assistence\CreateHandler::class] = function (Container $container) {
            return new Handler\Entity\CreateEntityHandler(
                $container[DomainWrite\AssistenceRepositoryInterface::class],
                $container['assistence.form']
            );
        };

        $container[Handler\Assistence\UpdateHandler::class] = function (Container $container) {
            return new Handler\Entity\UpdateEntityHandler(
                $container[DomainWrite\AssistenceRepositoryInterface::class],
                $container['assistence.form']
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerCurrencyHandler(Container $container)
    {
        $container[Handler\Currency\ListHandler::class] = function (Container $container) {
            return new Handler\Entity\ListEntityHandler(
                $container[DomainRead\CurrencyRepositoryInterface::class]
            );
        };

        $container[Handler\Currency\ShowHandler::class] = function (Container $container) {
            return new Handler\Entity\ShowEntityHandler(
                $container[DomainRead\CurrencyRepositoryInterface::class]
            );
        };

        $container[Handler\Currency\DeleteHandler::class] = function (Container $container) {
            return new Handler\Entity\DeleteEntityHandler(
                $container[DomainWrite\CurrencyRepositoryInterface::class]
            );
        };

        $container[Handler\Currency\CreateHandler::class] = function (Container $container) {
            return new Handler\Entity\CreateEntityHandler(
                $container[DomainWrite\CurrencyRepositoryInterface::class],
                $container['currency.form']
            );
        };

        $container[Handler\Currency\UpdateHandler::class] = function (Container $container) {
            return new Handler\Entity\UpdateEntityHandler(
                $container[DomainWrite\CurrencyRepositoryInterface::class],
                $container['currency.form']
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerContactUploadHandlers(Container $container)
    {
        $container[Handler\ContactUpload\ListHandler::class] = function (Container $container) {
            return new Handler\Entity\ListEntityHandler(
                $container[DomainRead\ContactUploadRepositoryInterface::class]
            );
        };

        $container[Handler\ContactUpload\ShowHandler::class] = function (Container $container) {
            return new Handler\Entity\ShowEntityHandler(
                $container[DomainRead\ContactUploadRepositoryInterface::class]
            );
        };

        $container[Handler\ContactUpload\DeleteHandler::class] = function (Container $container) {
            return new Handler\Entity\DeleteEntityHandler(
                $container[DomainWrite\ContactUploadRepositoryInterface::class]
            );
        };

        $container[Handler\ContactUpload\CreateHandler::class] = function (Container $container) {
            return new Handler\Entity\CreateEntityHandler(
                $container[DomainWrite\ContactUploadRepositoryInterface::class],
                $container['contact.upload.form']
            );
        };

        $container[Handler\ContactUpload\UpdateHandler::class] = function (Container $container) {
            return new Handler\Entity\UpdateEntityHandler(
                $container[DomainWrite\ContactUploadRepositoryInterface::class],
                $container['contact.upload.form']
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerPaymentHandlers(Container $container)
    {
        $container[Handler\Payment\ListHandler::class] = function (Container $container) {
            return new Handler\Entity\ListEntityHandler(
                $container[DomainRead\PaymentRepositoryInterface::class]
            );
        };

        $container[Handler\Payment\ShowHandler::class] = function (Container $container) {
            return new Handler\Entity\ShowEntityHandler(
                $container[DomainRead\PaymentRepositoryInterface::class]
            );
        };

        $container[Handler\Payment\DeleteHandler::class] = function (Container $container) {
            return new Handler\Entity\DeleteEntityHandler(
                $container[DomainWrite\PaymentRepositoryInterface::class]
            );
        };

        $container[Handler\Payment\CreateHandler::class] = function (Container $container) {
            return new Handler\Payment\CreateHandler(
                $container[DomainWrite\PaymentRepositoryInterface::class],
                $container['payment.form'],
                $container['dispatcher']
            );
        };

        $container[Handler\Payment\UpdateHandler::class] = function (Container $container) {
            return new Handler\Entity\UpdateEntityHandler(
                $container[DomainWrite\PaymentRepositoryInterface::class],
                $container['payment.form']
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerPaymentParcelsHandlers(Container $container)
    {
        $container[Handler\PaymentParcels\DeleteHandler::class] = function (Container $container) {
            return new Handler\Entity\DeleteEntityHandler(
                $container[DomainWrite\PaymentParcelRepositoryInterface::class]
            );
        };

        $container[Handler\PaymentParcels\ShowHandler::class] = function (Container $container) {
            return new Handler\Entity\ShowEntityHandler(
                $container[DomainRead\PaymentParcelRepositoryInterface::class]
            );
        };

        $container[Handler\PaymentParcels\ListHandler::class] = function (Container $container) {
            return new Handler\Entity\ListEntityHandler(
                $container[DomainRead\PaymentParcelRepositoryInterface::class]
            );
        };

        $container[Handler\PaymentParcels\CreateHandler::class] = function (Container $container) {
            return new Handler\Entity\CreateEntityHandler(
                $container[DomainWrite\PaymentParcelRepositoryInterface::class],
                $container['payment.parcels.form']
            );
        };

        $container[Handler\PaymentParcels\UpdateHandler::class] = function (Container $container) {
            return new Handler\Entity\UpdateEntityHandler(
                $container[DomainWrite\PaymentParcelRepositoryInterface::class],
                $container['payment.parcels.form']
            );
        };
    }
}
