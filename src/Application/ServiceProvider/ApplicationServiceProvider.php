<?php

namespace Cidadania\Application\ServiceProvider;

use Cidadania\Domain\Listener\PaymentListener;
use Cidadania\Domain\Repository\Read\CurrencyRepositoryInterface;
use Cidadania\Domain\Repository\Read\PartnerRepositoryInterface;
use Cidadania\Domain\Repository\Read\AssistenceRepositoryInterface;
use Cidadania\Domain\Repository\Write\PaymentParcelRepositoryInterface;
use Pimple\Container;
use Cidadania\Application\Form;
use Pimple\ServiceProviderInterface;
use Cidadania\Application\Controller;
use Cidadania\Domain\Repository\Read\ServiceRepositoryInterface;

class ApplicationServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $container)
    {
        $this->registerController($container);
        $this->registerForm($container);
        $this->registerEventListeners($container);
    }

    /**
     * @param Container $container
     */
    private function registerController(Container $container)
    {
        $container[Controller\AuthController::class] = function ($container) {
            return new Controller\AuthController(
                $container['bus']
            );
        };

        $container[Controller\ContactController::class] = function ($container) {
            return new Controller\ContactController(
                $container['bus']
            );
        };

        $container[Controller\CurrencyController::class] = function ($container) {
            return new Controller\CurrencyController(
                $container['bus']
            );
        };

        $container[Controller\PartnerController::class] = function ($container) {
            return new Controller\PartnerController(
                $container['bus']
            );
        };

        $container[Controller\ServiceController::class] = function ($container) {
            return new Controller\ServiceController(
                $container['bus']
            );
        };

        $container[Controller\AssistenceController::class] = function ($container) {
            return new Controller\AssistenceController(
                $container['bus']
            );
        };

        $container[Controller\PaymentController::class] = function ($container) {
            return new Controller\PaymentController(
                $container['bus']
            );
        };

        $container[Controller\LoginController::class] = function ($container) {
            return new Controller\LoginController(
                $container['bus']
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerForm(Container $container)
    {
        $container[Form\AuthForm::class] = function ($container) {
            return new Form\AuthForm(
                $container['form.factory']
            );
        };

        $container['auth.form'] = function ($container) {
            return new Form\Form(
                $container['form.factory'],
                Form\AuthForm::class
            );
        };

        $container[Form\ContactForm::class] = function ($container) {
            return new Form\ContactForm(
                $container[PartnerRepositoryInterface::class],
                $container[AssistenceRepositoryInterface::class]
            );
        };

        $container['contact.form'] = function ($container) {
            return new Form\Form(
                $container['form.factory'],
                Form\ContactForm::class
            );
        };

        $container[Form\CurrencyForm::class] = function () {
            return new Form\CurrencyForm();
        };

        $container['currency.form'] = function ($container) {
            return new Form\Form(
                $container['form.factory'],
                Form\CurrencyForm::class
            );
        };

        $container[Form\PartnerForm::class] = function ($container) {
            return new Form\PartnerForm(
                $container[ServiceRepositoryInterface::class]
            );
        };

        $container['partner.form'] = function ($container) {
            return new Form\Form(
                $container['form.factory'],
                Form\PartnerForm::class
            );
        };

        $container[Form\ServiceForm::class] = function ($container) {
            return new Form\ServiceForm();
        };

        $container['service.form'] = function ($container) {
            return new Form\Form(
                $container['form.factory'],
                Form\ServiceForm::class
            );
        };

        $container[Form\AssistenceForm::class] = function ($container) {
            return new Form\AssistenceForm();
        };

        $container['assistence.form'] = function ($container) {
            return new Form\Form(
                $container['form.factory'],
                Form\AssistenceForm::class
            );
        };

        $container[Form\ContactUploadForm::class] = function ($container) {
            return new Form\ContactUploadForm();
        };

        $container['contact.upload.form'] = function ($container) {
            return new Form\Form(
                $container['form.factory'],
                Form\ContactUploadForm::class
            );
        };

        $container[Form\PaymentForm::class] = function ($container) {
            return new Form\PaymentForm(
                $container[CurrencyRepositoryInterface::class]
            );
        };

        $container['payment.form'] = function ($container) {
            return new Form\Form(
                $container['form.factory'],
                Form\PaymentForm::class
            );
        };

        $container[Form\PaymentParcelsForm::class] = function ($container) {
            return new Form\PaymentParcelsForm();
        };

        $container['payment.parcels.form'] = function ($container) {
            return new Form\Form(
                $container['form.factory'],
                Form\PaymentParcelsForm::class
            );
        };
    }

    /**
     * @param Container $container
     */
    private function registerEventListeners(Container $container)
    {
        $container[PaymentListener::class] = function ($container) {
            return new PaymentListener(
                $container['bus']
            );
        };
    }
}
