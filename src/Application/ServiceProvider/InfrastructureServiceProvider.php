<?php

namespace Cidadania\Application\ServiceProvider;

use Cidadania\Domain\Repository\Read as DomainRead;
use Cidadania\Infrastructure\Repository\Read as InfrastructureRead;
use Cidadania\Domain\Repository\Write as DomainWrite;
use Cidadania\Infrastructure\Repository\Write as InfrastructureWrite;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class InfrastructureServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $container)
    {
        $this->registerReadRepository($container);
        $this->registerWriteRepository($container);
    }

    /**
     * @param Container $container
     */
    private function registerReadRepository(Container $container)
    {
        $container[DomainRead\ContactRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureRead\ContactRepository($container['db']);
        };

        $container[DomainRead\PartnerRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureRead\PartnerRepository(
                $container['db']
            );
        };

        $container[DomainRead\ServiceRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureRead\ServiceRepository($container['db']);
        };

        $container[DomainRead\AssistenceRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureRead\AssistenceRepository($container['db']);
        };

        $container[DomainRead\AuthRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureRead\AuthRepository($container['db']);
        };

        $container[DomainRead\ContactUploadRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureRead\ContactUploadRepository($container['db']);
        };

        $container[DomainRead\CurrencyRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureRead\CurrencyRepository($container['db']);
        };

        $container[DomainRead\PaymentRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureRead\PaymentRepository(
                $container['db']
            );
        };

        $container[DomainRead\PaymentParcelRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureRead\PaymentParcelRepository($container['db']);
        };
    }

    /**
     * @param Container $container
     */
    private function registerWriteRepository(Container $container)
    {
        $container[DomainWrite\AuthRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureWrite\AuthRepository($container['db']);
        };

        $container[DomainWrite\ContactRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureWrite\ContactRepository($container['db']);
        };

        $container[DomainWrite\CurrencyRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureWrite\CurrencyRepository($container['db']);
        };

        $container[DomainWrite\PartnerRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureWrite\PartnerRepository($container['db']);
        };

        $container[DomainWrite\ServiceRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureWrite\ServiceRepository($container['db']);
        };

        $container[DomainWrite\AssistenceRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureWrite\AssistenceRepository($container['db']);
        };

        $container[DomainWrite\ContactUploadRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureWrite\ContactUploadRepository($container['db']);
        };

        $container[DomainWrite\PaymentRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureWrite\PaymentRepository($container['db']);
        };

        $container[DomainWrite\PaymentParcelRepositoryInterface::class] = function (Container $container) {
            return new InfrastructureWrite\PaymentParcelRepository($container['db']);
        };
    }
}
