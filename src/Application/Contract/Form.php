<?php

namespace Cidadania\Application\Contract;

interface Form
{
    /**
     * @param array $payload
     * @param mixed|null $default The initial data, usually an entity.
     *
     * @return mixed The final data, usually an entity.
     */
    public function submit(array $payload, $default = null);
}
