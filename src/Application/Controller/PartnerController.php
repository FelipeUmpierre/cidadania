<?php

namespace Cidadania\Application\Controller;

use Cidadania\Business\Command\Partner\CreateCommand;
use Cidadania\Business\Command\Partner\DeleteCommand;
use Cidadania\Business\Command\Partner\ListCommand;
use Cidadania\Business\Command\Partner\ShowCommand;
use Cidadania\Business\Command\Partner\UpdateCommand;
use League\Tactician\CommandBus;
use Silex\Application;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class PartnerController
{
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * PartnerController constructor.
     *
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function index(Application $application)
    {
        $partners = $this->bus->handle(new ListCommand());

        return $application['twig']->render(
            'partner/index.html.twig',
            compact('partners')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return mixed
     */
    public function edit(Request $request, Application $application)
    {
        $partner = $this->bus->handle(new ShowCommand($request->get('id')));

        /** @var FormView $form */
        $form = $application['partner.form']->createForm([
            'action' => $application['routes']->get('partner.update')->getPath(),
            'data' => $partner,
        ])->createView();

        return $application['twig']->render(
            'partner/edit.html.twig',
            compact('partner', 'form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function update(Request $request, Application $application): RedirectResponse
    {
        $payload = $request->request->get('partner_form');

        $this->bus->handle(
            new UpdateCommand(
                $payload['id'],
                $payload
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Partner updated with success!'
        );

        return $application->redirect(
            $application['routes']->get('partner.index')->getPath()
        );
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function new(Application $application)
    {
        /** @var FormView $form */
        $form = $application['partner.form']->createForm([
            'action' => $application['routes']->get('partner.create')->getPath(),
        ])->createView();

        return $application['twig']->render(
            'partner/new.html.twig',
            compact('form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function create(Request $request, Application $application): RedirectResponse
    {
        $this->bus->handle(
            new CreateCommand(
                $request->request->get('partner_form')
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Partner created with success!'
        );

        return $application->redirect(
            $application['routes']->get('partner.index')->getPath()
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function delete(Request $request, Application $application): RedirectResponse
    {
        $this->bus->handle(
            new DeleteCommand(
                $request->get('id')
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Partner deleted with success!'
        );

        return $application->redirect(
            $application['routes']->get('partner.index')->getPath()
        );
    }
}
