<?php

namespace Cidadania\Application\Controller;

use Cidadania\Business\Command\Auth\AuthLoginCommand;
use League\Tactician\CommandBus;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class LoginController
{
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * LoginController constructor.
     *
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function index(Application $application)
    {
        return $application['twig']->render(
            'login/index.html.twig'
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function authenticate(Request $request, Application $application)
    {
        $this->bus->handle(
            new AuthLoginCommand(
                $request->request->get('username'),
                $request->request->get('password')
            )
        );

        $uri = 'auth.index';
        if ($application['session']->has('auth')) {
            $uri = 'contact.index';
        }

        return $application->redirect(
            $application['routes']->get($uri)->getPath()
        );
    }

    /**
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout(Application $application)
    {
        $application['session']->remove('auth');

        return $application->redirect(
            $application['routes']->get('auth.index')->getPath()
        );
    }
}
