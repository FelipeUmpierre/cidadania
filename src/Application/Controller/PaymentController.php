<?php

namespace Cidadania\Application\Controller;

use Cidadania\Business\Command\Currency\ShowCommand as CurrencyShowCommand;
use Cidadania\Business\Command\Payment\CreateCommand;
use Cidadania\Business\Command\Payment\ListCommand;
use Cidadania\Business\Command\Payment\ShowCommand;
use Cidadania\Business\Command\PaymentParcels\ListCommand as PaymentParcelsListCommand;
use Cidadania\Business\Command\PaymentParcels\ShowCommand as PaymentParcelShowCommand;
use Cidadania\Business\Command\PaymentParcels\UpdateCommand;
use Cidadania\Domain\Model\Payment;
use Cidadania\Domain\Model\PaymentParcels;
use League\Tactician\CommandBus;
use Silex\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class PaymentController
{
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * PaymentController constructor.
     *
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    public function index(Request $request, Application $application)
    {
        $contactID = $request->get('id');

        $payments = $this->bus->handle(
            new ListCommand([
                Payment::CONTACT => $contactID
            ])
        );

        $payment = array_pop($payments);

        $parcels = 0;
        $currency = '';
        if ($payment instanceof Payment && $payment->getId() !== null) {
            $parcels = $this->bus->handle(
                new PaymentParcelsListCommand([
                    PaymentParcels::PAYMENT => $payment->getId(),
                ])
            );

            $currency = $this->bus->handle(
                new CurrencyShowCommand(
                    $payment->getCurrency()
                )
            );
        }

        $form = $application['payment.form']->createForm([
            'action' => $application['routes']->get('payment.contact.modify')->getPath(),
            'data' => $payment,
        ])->createView();

        return $application['twig']->render(
            'payment/index.html.twig',
            compact('payment', 'parcels', 'form', 'contactID', 'currency')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function modify(Request $request, Application $application): RedirectResponse
    {
        $params = $request->request->get('payment_form');

        $this->bus->handle(
            new CreateCommand(
                $params
            )
        );

        return $application->redirect(
            $application['url_generator']->generate('payment.contact.index', ['id' => $params['contact']])
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function paid(Request $request, Application $application): RedirectResponse
    {
        $payment = $this->updatePaidStatus($request);

        return $application->redirect(
            $application['url_generator']->generate('payment.contact.index', ['id' => $payment->getContact()])
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function unpaid(Request $request, Application $application): RedirectResponse
    {
        $payment = $this->updatePaidStatus($request, false);

        return $application->redirect(
            $application['url_generator']->generate('payment.contact.index', ['id' => $payment->getContact()])
        );
    }

    /**
     * @param Request $request
     * @param bool $paid
     */
    private function updatePaidStatus(Request $request, $paid = true): Payment
    {
        /** @var PaymentParcels $parcel */
        $parcel = $this->bus->handle(
            new PaymentParcelShowCommand(
                $request->attributes->get('id')
            )
        );

        /** @var Payment $payment */
        $payment = $this->bus->handle(
            new ShowCommand(
                $parcel->getPayment()
            )
        );

        $parcel->setIsPaid($paid);

        $this->bus->handle(
            new UpdateCommand(
                $parcel->getId(),
                $parcel->toArray()
            )
        );

        return $payment;
    }
}
