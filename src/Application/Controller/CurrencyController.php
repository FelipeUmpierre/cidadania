<?php

namespace Cidadania\Application\Controller;

use Cidadania\Business\Command\Currency\CreateCommand;
use Cidadania\Business\Command\Currency\DeleteCommand;
use Cidadania\Business\Command\Currency\ListCommand;
use Cidadania\Business\Command\Currency\ShowCommand;
use Cidadania\Business\Command\Currency\UpdateCommand;
use League\Tactician\CommandBus;
use Silex\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CurrencyController
{
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * ServiceController constructor.
     *
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function index(Application $application)
    {
        $currencies = $this->bus->handle(new ListCommand());

        return $application['twig']->render(
            'currency/index.html.twig',
            compact('currencies')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Request $request, Application $application)
    {
        $currency = $this->bus->handle(
            new ShowCommand(
                $request->get('id')
            )
        );

        $form = $application['currency.form']->createForm([
            'action' => $application['routes']->get('currency.update')->getPath(),
            'data' => $currency,
        ])->createView();

        return $application['twig']->render(
            'currency/edit.html.twig',
            compact('currency', 'form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update(Request $request, Application $application)
    {
        $params = $request->request->get('currency_form');

        $this->bus->handle(
            new UpdateCommand(
                $params['id'],
                $params
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Currency updated with success!'
        );

        return $application->redirect(
            $application['routes']->get('currency.index')->getPath()
        );
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function new(Application $application)
    {
        $form = $application['currency.form']->createForm([
            'action' => $application['routes']->get('currency.create')->getPath(),
        ])->createView();

        return $application['twig']->render(
            'currency/new.html.twig',
            compact('form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function create(Request $request, Application $application): RedirectResponse
    {
        $params = $request->request->get('currency_form');

        $this->bus->handle(
            new CreateCommand($params)
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Currency created with success!'
        );

        return $application->redirect(
            $application['routes']->get('currency.index')->getPath()
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function delete(Request $request, Application $application): RedirectResponse
    {
        $this->bus->handle(
            new DeleteCommand(
                $request->get('id')
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Currency deleted with success!'
        );

        return $application->redirect(
            $application['routes']->get('currency.index')->getPath()
        );
    }
}
