<?php

namespace Cidadania\Application\Controller;

use Cidadania\Application\Form\ServiceForm;
use Cidadania\Business\Command\Service\CreateCommand;
use Cidadania\Business\Command\Service\DeleteCommand;
use Cidadania\Business\Command\Service\ListCommand;
use Cidadania\Business\Command\Service\ShowCommand;
use Cidadania\Business\Command\Service\UpdateCommand;
use Cidadania\Domain\Model\Service;
use League\Tactician\CommandBus;
use Silex\Application;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;

class ServiceController
{
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * ServiceController constructor.
     *
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function index(Application $application)
    {
        $services = $this->bus->handle(new ListCommand());

        return $application['twig']->render(
            'service/index.html.twig',
            compact('services')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Request $request, Application $application)
    {
        $service = $this->bus->handle(
            new ShowCommand(
                $request->get('id')
            )
        );

        /** @var FormView $form */
        $form = $application['service.form']->createForm([
            'action' => $application['routes']->get('service.update')->getPath(),
            'data' => $service,
        ])->createView();

        return $application['twig']->render(
            'service/edit.html.twig',
            compact('service', 'form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update(Request $request, Application $application)
    {
        $params = $request->request->get('service_form');

        $this->bus->handle(
            new UpdateCommand(
                $params['id'],
                $params
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Service updated with success!'
        );

        return $application->redirect(
            $application['routes']->get('service.index')->getPath()
        );
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function new(Application $application)
    {
        /** @var FormView $form */
        $form = $application['service.form']->createForm([
            'action' => $application['routes']->get('service.create')->getPath(),
        ])->createView();

        return $application['twig']->render(
            'service/new.html.twig',
            compact('form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function create(Request $request, Application $application)
    {
        $params = $request->request->get('service_form');

        $this->bus->handle(new CreateCommand($params));

        $application['session']->getFlashBag()->add(
            'success',
            'Service created with success!'
        );

        return $application->redirect(
            $application['routes']->get('service.index')->getPath()
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, Application $application)
    {
        $this->bus->handle(
            new DeleteCommand(
                $request->get('id')
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Service deleted with success'
        );

        return $application->redirect(
            $application['routes']->get('service.index')->getPath()
        );
    }
}
