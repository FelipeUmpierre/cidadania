<?php

namespace Cidadania\Application\Controller;

use Cidadania\Business\Command\Contact\CreateCommand;
use Cidadania\Business\Command\Contact\DeleteCommand;
use Cidadania\Business\Command\Contact\ListCommand;
use Cidadania\Business\Command\Contact\ShowCommand;
use Cidadania\Business\Command\Contact\UpdateCommand;
use Cidadania\Business\Command\ContactUpload\CreateCommand as UploadedFileCreateCommand;
use Cidadania\Business\Command\ContactUpload\ListCommand as UploadedFileListCommand;
use Cidadania\Domain\Model\Contact;
use Cidadania\Domain\Model\ContactUpload;
use Collections\Vector;
use League\Tactician\CommandBus;
use Silex\Application;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ContactController
{
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * ContactController constructor.
     *
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function index(Application $application)
    {
        /** @var Vector $contacts */
        $contacts = $this->bus->handle(new ListCommand());

        return $application['twig']->render(
            'contact/index.html.twig',
            compact('contacts')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return mixed
     */
    public function edit(Request $request, Application $application)
    {
        /** @var Contact $contact */
        $contact = $this->bus->handle(
            new ShowCommand(
                $request->get('id')
            )
        );

        $uploadedFiles = $this->bus->handle(
            new UploadedFileListCommand([
                ContactUpload::CONTACT => $request->get('id')
            ])
        );

        $form = $application['contact.form']->createForm([
            'action' => $application['routes']->get('contact.update')->getPath(),
            'data' => $contact,
        ])->createView();

        $uploadForm = $application['contact.upload.form']->createForm([
            'action' => $application['routes']->get('contact.upload')->getPath(),
        ])->createView();

        return $application['twig']->render(
            'contact/edit.html.twig',
            compact('contact', 'form', 'uploadForm', 'uploadedFiles')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function update(Request $request, Application $application): RedirectResponse
    {
        $params = $request->request->get('contact_form');

        $this->bus->handle(
            new UpdateCommand(
                $params['id'],
                $params
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Contact updated with success!'
        );

        return $application->redirect(
            $application['routes']->get('contact.index')->getPath()
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    public function upload(Request $request, Application $application): RedirectResponse
    {
        $form = $request->files->get('contact_upload_form');
        $contactID = $request->request->get('form')[ContactUpload::CONTACT];

        if (empty($form['file'])) {
            $application['session']->getFlashBag()->add(
                'error',
                'No file was uploaded, because you did not select any file.'
            );
            
            return $application->redirect(
                $application['url_generator']->generate('contact.edit', [
                    'id' => $contactID,
                ])
            );
        }

        /** @var UploadedFile $file */
        foreach ($form['file'] as $file) {
            if (!$file->isValid()) {
                continue;
            }

            $file->move(
                $application['upload.path'],
                $file->getClientOriginalName()
            );

            $this->bus->handle(
                new UploadedFileCreateCommand([
                    ContactUpload::CONTACT => $contactID,
                    ContactUpload::FILE => $file->getClientOriginalName()
                ])
            );
        }

        $application['session']->getFlashBag()->add(
            'success',
            'File(s) uploaded with success'
        );

        return $application->redirect(
            $application['url_generator']->generate('contact.edit', [
                'id' => $contactID,
            ])
        );
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function new(Application $application)
    {
        $form = $application['contact.form']->createForm([
            'action' => $application['routes']->get('contact.create')->getPath(),
        ])->createView();

        return $application['twig']->render(
            'contact/new.html.twig',
            compact('form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function create(Request $request, Application $application): RedirectResponse
    {
        $params = $request->request->get('contact_form');

        $this->bus->handle(new CreateCommand($params));

        $application['session']->getFlashBag()->add(
            'success',
            'Contact created with success!'
        );

        return $application->redirect(
            $application['routes']->get('contact.index')->getPath()
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function delete(Request $request, Application $application): RedirectResponse
    {
        $this->bus->handle(
            new DeleteCommand(
                $request->get('id')
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Contact deleted with success!'
        );

        return $application->redirect(
            $application['routes']->get('contact.index')->getPath()
        );
    }
}
