<?php

namespace Cidadania\Application\Controller;

use Cidadania\Application\Form\AssistenceForm;
use Cidadania\Business\Command\Assistence\CreateCommand;
use Cidadania\Business\Command\Assistence\DeleteCommand;
use Cidadania\Business\Command\Assistence\ListCommand;
use Cidadania\Business\Command\Assistence\ShowCommand;
use Cidadania\Business\Command\Assistence\UpdateCommand;
use Cidadania\Domain\Model\Assistence;
use League\Tactician\CommandBus;
use Silex\Application;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;

class AssistenceController
{
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * AssistenceController constructor.
     *
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function index(Application $application)
    {
        $assistences = $this->bus->handle(new ListCommand());

        return $application['twig']->render(
            'assistence/index.html.twig',
            compact('assistences')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Request $request, Application $application)
    {
        $assistence = $this->bus->handle(
            new ShowCommand(
                $request->get('id')
            )
        );

        /** @var FormView $form */
        $form = $application['assistence.form']->createForm([
            'action' => $application['routes']->get('assistence.update')->getPath(),
            'data' => $assistence,
        ])->createView();

        return $application['twig']->render(
            'assistence/edit.html.twig',
            compact('assistence', 'form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update(Request $request, Application $application)
    {
        $params = $request->request->get('assistence_form');

        $this->bus->handle(
            new UpdateCommand(
                $params['id'],
                $params
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Assistence updated with success!'
        );

        return $application->redirect(
            $application['routes']->get('assistence.index')->getPath()
        );
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function new(Application $application)
    {
        /** @var FormView $form */
        $form = $application['assistence.form']->createForm([
            'action' => $application['routes']->get('assistence.create')->getPath(),
        ])->createView();

        return $application['twig']->render(
            'assistence/new.html.twig',
            compact('form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function create(Request $request, Application $application)
    {
        $params = $request->request->get('assistence_form');

        $this->bus->handle(new CreateCommand($params));

        $application['session']->getFlashBag()->add(
            'success',
            'Assistence created with success!'
        );

        return $application->redirect(
            $application['routes']->get('assistence.index')->getPath()
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, Application $application)
    {
        $this->bus->handle(
            new DeleteCommand(
                $request->get('id')
            )
        );

        $application['session']->getFlashBag()->add(
            'success',
            'Assistence deleted with success'
        );

        return $application->redirect(
            $application['routes']->get('assistence.index')->getPath()
        );
    }
}
