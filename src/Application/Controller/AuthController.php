<?php

namespace Cidadania\Application\Controller;

use Cidadania\Application\Form\AuthForm;
use Cidadania\Business\Command\Auth\ListAuthCommand;
use Cidadania\Business\Command\Auth\WriteAuthCommand;
use Cidadania\Domain\Model\Auth;
use League\Tactician\CommandBus;
use Silex\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AuthController
{
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * AuthController constructor.
     *
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function index(Application $application)
    {
        $users = $this->bus->handle(new ListAuthCommand(null));

        return $application['twig']->render(
            'user/index.html.twig',
            compact('users')
        );
    }

    /**
     * @param Application $application
     *
     * @return mixed
     */
    public function new(Application $application)
    {
        $form = $application[AuthForm::class]->buildForm(
            new Auth(),
            $application['routes']->get('user.create')->getPath()
        )->createView();

        return $application['twig']->render(
            'user/new.html.twig',
            compact('form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function create(Request $request, Application $application): RedirectResponse
    {
        $params = $request->request->get('form');

        $this->bus->handle(WriteAuthCommand::create($params));

        $application['session']->getFlashBag()->add(
            'success',
            'User created with success'
        );

        return $application->redirect(
            $application['routes']->get('user.index')->getPath()
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return mixed
     */
    public function edit(Request $request, Application $application)
    {
        $user = $this->bus->handle(
            new ListAuthCommand(
                $request->get('id')
            )
        );

        if ($user === new Auth()) {
            $application['session']->getFlashBah()->add(
                'error',
                'No user found'
            );

            return $application->redirect(
                $application['routes']->get('user.index')->getPath()
            );
        }

        $form = $application[AuthForm::class]->buildForm(
            $user,
            $application['routes']->get('user.update')->getPath()
        )->createView();

        return $application['twig']->render(
            'user/edit.html.twig',
            compact('user', 'form')
        );
    }

    /**
     * @param Request $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function update(Request $request, Application $application): RedirectResponse
    {
        $params = $request->request->get('form');

        $this->bus->handle(WriteAuthCommand::create($params));

        $application['session']->getFlashBag()->add(
            'success',
            'User updated with success!'
        );

        return $application->redirect(
            $application['routes']->get('user.index')->getPath()
        );
    }
}