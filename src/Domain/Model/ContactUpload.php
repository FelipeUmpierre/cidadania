<?php

namespace Cidadania\Domain\Model;

final class ContactUpload extends EntityAbstract
{
    const ID = 'id';
    const CONTACT = 'fk_contact';
    const FILE = 'file';
    const CREATED_AT = 'created_at';

    /**
     * @var int
     */
    private $contact;

    /**
     * @var string
     */
    private $file;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param array $payload
     *
     * @return ContactUpload
     */
    public static function create(array $payload): ContactUpload
    {
        $upload = new self;
        $upload->setId($payload[self::ID]);
        $upload->setFkContact($payload[self::CONTACT]);
        $upload->setFile($payload[self::FILE]);
        $upload->setCreatedAt($payload[self::CREATED_AT]);

        return $upload;
    }

    /**
     * @return int
     */
    public function getFkContact(): ?int
    {
        return $this->contact;
    }

    /**
     * @param int $contact
     */
    public function setFkContact(int $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile(string $file)
    {
        $this->file = $file;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = \DateTimeImmutable::createFromFormat(DATE_MYSQL_FORMAT, $createdAt)->format(DATE_MYSQL_FORMAT);
    }

    /**
     * @return string
     */
    public function getIdentifierName(): string
    {
        return self::ID;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::ID => $this->getId(),
            self::CONTACT => $this->getFkContact(),
            self::FILE => $this->getFile(),
            self::CREATED_AT => $this->getCreatedAt(),
        ];
    }
}
