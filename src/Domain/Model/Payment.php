<?php

namespace Cidadania\Domain\Model;

final class Payment extends EntityAbstract
{
    const ID = 'id';
    const CURRENCY = 'fk_currency';
    const CONTACT = 'fk_contact';
    const PARCELS = 'parcels';
    const PRICE = 'price';
    const PARTNER_PRICE = 'partner_price';
    const TOTAL_PRICE = 'total_price';
    const CREATED_AT = 'created_at';
    const START_PAYMENT = 'start_payment';

    /**
     * @var int
     */
    private $currency;

    /**
     * @var int
     */
    private $contact;

    /**
     * @var int
     */
    private $parcels;

    /**
     * @var float
     */
    private $price;

    /**
     * @var float
     */
    private $partnerPrice;

    /**
     * @var float
     */
    private $totalPrice;

    /**
     * @var string
     */
    private $createdAt;

    /**
     * @var string
     */
    private $startPayment;

    /**
     * @param array $payload
     *
     * @return Payment
     */
    public static function create(array $payload): self
    {
        $payment = new self;
        $payment->setId($payload[self::ID]);
        $payment->setCurrency($payload[self::CURRENCY]);
        $payment->setContact($payload[self::CONTACT]);
        $payment->setParcels($payload[self::PARCELS]);
        $payment->setPrice($payload[self::PRICE]);
        $payment->setPartnerPrice($payload[self::PARTNER_PRICE]);
        $payment->setTotalPrice($payload[self::TOTAL_PRICE]);
        $payment->setStartPayment($payload[self::START_PAYMENT]);
        $payment->setCreatedAt($payload[self::CREATED_AT]);

        return $payment;
    }

    /**
     * @return int
     */
    public function getCurrency(): ?int
    {
        return $this->currency;
    }

    /**
     * @param int $currency
     */
    public function setCurrency(int $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getContact(): ?int
    {
        return $this->contact;
    }

    /**
     * @param int $contact
     */
    public function setContact(int $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return int
     */
    public function getParcels(): ?int
    {
        return $this->parcels;
    }

    /**
     * @param int $parcels
     */
    public function setParcels(int $parcels)
    {
        $this->parcels = $parcels;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getPartnerPrice(): ?float
    {
        return $this->partnerPrice;
    }

    /**
     * @param float $partnerPrice
     */
    public function setPartnerPrice(float $partnerPrice)
    {
        $this->partnerPrice = $partnerPrice;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): ?float
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     */
    public function setTotalPrice(?float $totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = \DateTimeImmutable::createFromFormat(DATE_MYSQL_FORMAT, $createdAt)->format('Y-m-d');
    }

    /**
     * @return string
     */
    public function getStartPayment(): ?string
    {
        return $this->startPayment;
    }

    /**
     * @param string $startPayment
     */
    public function setStartPayment(string $startPayment)
    {
        $this->startPayment = $startPayment;
    }

    /**
     * @return string
     */
    public function getIdentifierName(): string
    {
        return self::ID;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::ID => $this->getId(),
            self::CURRENCY => $this->getCurrency(),
            self::CONTACT => $this->getContact(),
            self::PARCELS => $this->getParcels(),
            self::PRICE => $this->getPrice(),
            self::PARTNER_PRICE => $this->getPartnerPrice(),
            self::TOTAL_PRICE => $this->getTotalPrice(),
            self::CREATED_AT => $this->getCreatedAt(),
            self::START_PAYMENT => $this->getStartPayment(),
        ];
    }
}
