<?php

namespace Cidadania\Domain\Model;

abstract class EntityAbstract implements Entity
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @param int|null $id
     *
     * @return $this
     */
    public function setId(?int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function __construct(?int $id = null)
    {
        $this->setId($id);
    }
}
