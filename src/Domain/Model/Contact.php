<?php

namespace Cidadania\Domain\Model;

final class Contact extends EntityAbstract
{
    const ID = 'id';
    const PARTNER = 'fk_partner';
    const ASSISTENCE = 'fk_assistence';
    const CODE = 'code';
    const NAME = 'name';
    const EMAIL = 'email';
    const PHONE = 'phone';
    const BIRTHDAY = 'birthday';
    const ADDRESS = 'address';
    const PASSPORT = 'passport';
    const SCHOOLING = 'schooling';
    const UK_ENTRANCE_DATE = 'uk_entrance_date';
    const FATHER_NAME = 'father_name';
    const MOTHER_NAME = 'mother_name';
    const PROFESSION = 'profession';
    const OBSERVATION = 'observation';
    const CITY = 'city';
    const IS_CLIENT = 'is_client';
    const IS_DONE = 'is_done';
    const CONTACTED_AT = 'contacted_at';
    const DELETED_AT = 'deleted_at';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @var int
     */
    private $partner;

    /**
     * @var int
     */
    private $assistence;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $birthday;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $passport;

    /**
     * @var string
     */
    private $schooling;

    /**
     * @var string
     */
    private $ukEntranceDate;

    /**
     * @var string
     */
    private $fatherName;

    /**
     * @var string
     */
    private $motherName;

    /**
     * @var string
     */
    private $profession;

    /**
     * @var string
     */
    private $observation;

    /**
     * @var string
     */
    private $city;

    /**
     * @var bool
     */
    private $isClient;

    /**
     * @var bool
     */
    private $isDone;

    /**
     * @var \DateTimeImmutable
     */
    private $contactedAt;

    /**
     * @var \DateTimeImmutable
     */
    private $deletedAt;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var \DateTimeImmutable
     */
    private $updatedAt;

    /**
     * @param array $payload
     *
     * @return Contact
     */
    public static function create(array $payload): self
    {
        $contact = new self;
        $contact->setId($payload[self::ID]);
        $contact->setPartner($payload[self::PARTNER]);
        $contact->setAssistence($payload[self::ASSISTENCE]);
        $contact->setCode($payload[self::CODE]);
        $contact->setName($payload[self::NAME]);
        $contact->setEmail($payload[self::EMAIL]);
        $contact->setPhone($payload[self::PHONE]);
        $contact->setBirthday($payload[self::BIRTHDAY]);
        $contact->setAddress($payload[self::ADDRESS]);
        $contact->setPassport($payload[self::PASSPORT]);
        $contact->setSchooling($payload[self::SCHOOLING]);
        $contact->setUkEntranceDate($payload[self::UK_ENTRANCE_DATE]);
        $contact->setFatherName($payload[self::FATHER_NAME]);
        $contact->setMotherName($payload[self::MOTHER_NAME]);
        $contact->setProfession($payload[self::PROFESSION]);
        $contact->setObservation($payload[self::OBSERVATION]);
        $contact->setCity($payload[self::CITY]);
        $contact->setIsClient($payload[self::IS_CLIENT]);
        $contact->setIsDone($payload[self::IS_DONE]);
        $contact->setContactedAt($payload[self::CONTACTED_AT]);
        $contact->setDeletedAt($payload[self::DELETED_AT]);
        $contact->setCreatedAt($payload[self::CREATED_AT]);
        $contact->setUpdatedAt($payload[self::UPDATED_AT]);

        return $contact;
    }

    /**
     * @return int
     */
    public function getPartner(): ?int
    {
        return $this->partner;
    }

    /**
     * @return string
     */
    public function getAssistence(): ?string
    {
        return $this->assistence;
    }

    /**
     * @param int $partner
     */
    public function setPartner(int $partner)
    {
        $this->partner = $partner;
    }

    /**
     * @param string $partner
     */
    public function setAssistence(?string $assistence)
    {
        $this->assistence = $assistence;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(?string $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isIsClient(): ?bool
    {
        return $this->isClient;
    }

    /**
     * @param bool $isClient
     */
    public function setIsClient(bool $isClient)
    {
        $this->isClient = $isClient;
    }

    /**
     * @return bool
     */
    public function isIsDone(): ?bool
    {
        return $this->isDone;
    }

    /**
     * @param bool $isDone
     */
    public function setIsDone(bool $isDone)
    {
        $this->isDone = $isDone;
    }

    /**
     * @return string
     */
    public function getContactedAt(): ?string
    {
        return $this->contactedAt;
    }

    /**
     * @param string $contactedAt
     */
    public function setContactedAt(?string $contactedAt)
    {
        $this->contactedAt = $contactedAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    /**
     * @param string $deletedAt
     */
    public function setDeletedAt(?string $deletedAt)
    {
        if ($deletedAt !== null) {
            $this->deletedAt = \DateTimeImmutable::createFromFormat(DATE_MYSQL_FORMAT, $deletedAt)->format('Y-m-d');
        }
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = \DateTimeImmutable::createFromFormat(DATE_MYSQL_FORMAT, $createdAt)->format('Y-m-d');
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     */
    public function setUpdatedAt(string $updatedAt)
    {
        $this->updatedAt = \DateTimeImmutable::createFromFormat(DATE_MYSQL_FORMAT, $updatedAt)->format('Y-m-d');
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    /**
     * @param string $birthday
     */
    public function setBirthday(string $birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPassport(): ?string
    {
        return $this->passport;
    }

    /**
     * @param string $passport
     */
    public function setPassport(string $passport)
    {
        $this->passport = $passport;
    }

    /**
     * @return string
     */
    public function getSchooling(): ?string
    {
        return $this->schooling;
    }

    /**
     * @param string $schooling
     */
    public function setSchooling(string $schooling)
    {
        $this->schooling = $schooling;
    }

    /**
     * @return string
     */
    public function getUkEntranceDate(): ?string
    {
        return $this->ukEntranceDate;
    }

    /**
     * @param string $ukEntranceDate
     */
    public function setUkEntranceDate(string $ukEntranceDate)
    {
        $this->ukEntranceDate = $ukEntranceDate;
    }

    /**
     * @return string
     */
    public function getFatherName(): ?string
    {
        return $this->fatherName;
    }

    /**
     * @param string $fatherName
     */
    public function setFatherName(string $fatherName)
    {
        $this->fatherName = $fatherName;
    }

    /**
     * @return string
     */
    public function getMotherName(): ?string
    {
        return $this->motherName;
    }

    /**
     * @param string $motherName
     */
    public function setMotherName(string $motherName)
    {
        $this->motherName = $motherName;
    }

    /**
     * @return string
     */
    public function getProfession(): ?string
    {
        return $this->profession;
    }

    /**
     * @param string $profession
     */
    public function setProfession(string $profession)
    {
        $this->profession = $profession;
    }

    /**
     * @param string $observation
     */
    public function setObservation(string $observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return string
     */
    public function getObservation(): ?string
    {
        return $this->observation;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getIdentifierName(): string
    {
        return self::ID;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::ID => $this->getId(),
            self::PARTNER => $this->getPartner(),
            self::ASSISTENCE => $this->getAssistence(),
            self::CODE => $this->getCode(),
            self::NAME => $this->getName(),
            self::EMAIL => $this->getEmail(),
            self::PHONE => $this->getPhone(),
            self::BIRTHDAY => $this->getBirthday(),
            self::ADDRESS => $this->getAddress(),
            self::PASSPORT => $this->getPassport(),
            self::SCHOOLING => $this->getSchooling(),
            self::UK_ENTRANCE_DATE => $this->getUkEntranceDate(),
            self::FATHER_NAME => $this->getFatherName(),
            self::MOTHER_NAME => $this->getMotherName(),
            self::PROFESSION => $this->getProfession(),
            self::OBSERVATION => $this->getObservation(),
            self::CITY => $this->getCity(),
            self::IS_CLIENT => $this->isIsClient(),
            self::IS_DONE => $this->isIsDone(),
            self::CONTACTED_AT => $this->getContactedAt(),
            self::DELETED_AT => $this->getDeletedAt(),
            self::CREATED_AT => $this->getCreatedAt(),
            self::UPDATED_AT => $this->getUpdatedAt(),
        ];
    }
}
