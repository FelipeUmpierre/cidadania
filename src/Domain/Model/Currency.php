<?php

namespace Cidadania\Domain\Model;

final class Currency extends EntityAbstract
{
    const ID = 'id';
    const NAME = 'name';
    const SYMBOL = 'symbol';
    const CREATED_AT = 'created_at';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $symbol;

    /**
     * @var string
     */
    private $createdAt;

    /**
     * @param array $payload
     *
     * @return Currency
     */
    public static function create(array $payload): self
    {
        $currency = new self;
        $currency->setId($payload[self::ID]);
        $currency->setName($payload[self::NAME]);
        $currency->setSymbol($payload[self::SYMBOL]);
        $currency->setCreatedAt($payload[self::CREATED_AT]);

        return $currency;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     */
    public function setSymbol(string $symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(?string $createdAt)
    {
        $this->createdAt = \DateTimeImmutable::createFromFormat(
                DATE_MYSQL_FORMAT,
                $createdAt
            ) ?? (new \DateTime())->format(DATE_MYSQL_FORMAT);
    }

    /**
     * @return string
     */
    public function getIdentifierName(): string
    {
        return self::ID;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::ID => $this->getId(),
            self::NAME => $this->getName(),
            self::SYMBOL => $this->getSymbol(),
            self::CREATED_AT => $this->getCreatedAt(),
        ];
    }
}
