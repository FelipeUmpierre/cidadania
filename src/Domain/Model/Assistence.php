<?php

namespace Cidadania\Domain\Model;

final class Assistence extends EntityAbstract
{
    const ID = 'id';
    const NAME = 'name';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $createdAt;

    /**
     * @var string
     */
    private $updatedAt;

    /**
     * @param array $payload
     *
     * @return Assistence
     */
    public static function create(array $payload): self
    {
        $assistence = new self;
        $assistence->setId($payload[self::ID]);
        $assistence->setName($payload[self::NAME]);
        $assistence->setCreatedAt($payload[self::CREATED_AT]);
        $assistence->setUpdatedAt($payload[self::UPDATED_AT]);

        return $assistence;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(?string $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     */
    public function setUpdatedAt(?string $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getIdentifierName(): string
    {
        return self::ID;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::ID => $this->getId(),
            self::NAME => $this->getName(),
            self::CREATED_AT => $this->getCreatedAt(),
            self::UPDATED_AT => $this->getUpdatedAt(),
        ];
    }
}
