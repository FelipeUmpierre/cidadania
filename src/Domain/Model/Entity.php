<?php

namespace Cidadania\Domain\Model;

interface Entity
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    public function setId(?int $id);

    /**
     * @return string
     */
    public function getIdentifierName(): string;

    /**
     * @return array
     */
    public function toArray(): array;
}
