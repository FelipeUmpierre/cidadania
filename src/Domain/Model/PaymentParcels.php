<?php

namespace Cidadania\Domain\Model;

final class PaymentParcels extends EntityAbstract
{
    const ID = 'id';
    const PAYMENT = 'fk_payment';
    const PARCEL_PRICE = 'parcel_price';
    const IS_PAID = 'is_paid';
    const PAYMENT_DATE = 'payment_date';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @var int
     */
    private $payment;

    /**
     * @var float
     */
    private $parcelPrice;

    /**
     * @var bool
     */
    private $isPaid;

    /**
     * @var string
     */
    private $paymentDate;

    /**
     * @var string
     */
    private $createdAt;

    /**
     * @var string
     */
    private $updatedAt;

    /**
     * @param array $payload
     *
     * @return PaymentParcels
     */
    public static function create(array $payload): self
    {
        $paymentParcels = new self;
        $paymentParcels->setId($payload[self::ID]);
        $paymentParcels->setPayment($payload[self::PAYMENT]);
        $paymentParcels->setParcelPrice($payload[self::PARCEL_PRICE]);
        $paymentParcels->setIsPaid($payload[self::IS_PAID]);
        $paymentParcels->setPaymentDate($payload[self::PAYMENT_DATE]);
        $paymentParcels->setCreatedAt($payload[self::CREATED_AT]);
        $paymentParcels->setUpdatedAt($payload[self::UPDATED_AT]);

        return $paymentParcels;
    }

    /**
     * @return int
     */
    public function getPayment(): ?int
    {
        return $this->payment;
    }

    /**
     * @param int $payment
     */
    public function setPayment(int $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return float
     */
    public function getParcelPrice(): ?float
    {
        return $this->parcelPrice;
    }

    /**
     * @param float $parcelPrice
     */
    public function setParcelPrice(float $parcelPrice)
    {
        $this->parcelPrice = $parcelPrice;
    }

    /**
     * @return bool|null
     */
    public function isIsPaid(): ?bool
    {
        return $this->isPaid;
    }

    /**
     * @param bool $isPaid
     */
    public function setIsPaid(bool $isPaid)
    {
        $this->isPaid = $isPaid;
    }

    /**
     * @return string
     */
    public function getPaymentDate(): ?string
    {
        return $this->paymentDate;
    }

    /**
     * @param string $paymentDate
     */
    public function setPaymentDate(string $paymentDate)
    {
        $this->paymentDate = $paymentDate;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     */
    public function setUpdatedAt(string $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getIdentifierName(): string
    {
        return self::ID;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::ID => $this->getId(),
            self::PAYMENT => $this->getPayment(),
            self::PARCEL_PRICE => $this->getParcelPrice(),
            self::IS_PAID => $this->isIsPaid(),
            self::PAYMENT_DATE => $this->getPaymentDate(),
            self::CREATED_AT => $this->getCreatedAt(),
            self::UPDATED_AT => $this->getUpdatedAt(),
        ];
    }
}
