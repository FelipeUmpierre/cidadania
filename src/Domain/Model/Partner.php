<?php

namespace Cidadania\Domain\Model;

final class Partner extends EntityAbstract
{
    const ID = 'id';
    const SERVICE = 'fk_service';
    const NAME = 'name';
    const EMAIL = 'email';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @var int
     */
    private $service;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $createdAt;

    /**
     * @var string
     */
    private $updatedAt;

    /**
     * @param array $payload
     *
     * @return Partner
     */
    public static function create(array $payload): self
    {
        $partner = new self;
        $partner->setId($payload[self::ID]);
        $partner->setService($payload[self::SERVICE]);
        $partner->setName($payload[self::NAME]);
        $partner->setEmail($payload[self::EMAIL]);
        $partner->setCreatedAt($payload[self::CREATED_AT]);
        $partner->setUpdatedAt($payload[self::UPDATED_AT]);

        return $partner;
    }

    /**
     * @return int|null
     */
    public function getService(): ?int
    {
        return $this->service;
    }

    /**
     * @param int $service
     *
     * @return $this
     */
    public function setService(int $service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(string $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [
            self::ID => $this->getId(),
            self::SERVICE => $this->getService(),
            self::NAME => $this->getName(),
            self::EMAIL => $this->getEmail(),
            self::CREATED_AT => $this->getCreatedAt(),
            self::UPDATED_AT => $this->getUpdatedAt(),
        ];
    }

    /**
     * @return string
     */
    public function getIdentifierName(): string
    {
        return self::ID;
    }
}
