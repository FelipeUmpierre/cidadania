<?php

namespace Cidadania\Domain\Model;

final class Auth extends EntityAbstract
{
    const ID = 'id';
    const USERNAME = 'username';
    const PASSWORD = 'password';
    const ROLES = 'roles';

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $roles;

    /**
     * @param array $payload
     *
     * @return Auth
     */
    public static function create(array $payload): Auth
    {
        $auth = new self;
        $auth->setId($payload[self::ID]);
        $auth->setUsername($payload[self::USERNAME]);
        $auth->setPassword($payload[self::PASSWORD]);
        $auth->setRoles($payload[self::ROLES]);

        return $auth;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getRoles(): string
    {
        return $this->roles;
    }

    /**
     * @param string $roles
     */
    public function setRoles(string $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return string
     */
    public function getIdentifierName(): string
    {
        return self::ID;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::ID => $this->getId(),
            self::USERNAME => $this->getUsername(),
            self::PASSWORD => $this->getPassword(),
            self::ROLES => $this->getRoles(),
        ];
    }
}
