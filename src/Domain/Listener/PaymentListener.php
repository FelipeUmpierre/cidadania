<?php

namespace Cidadania\Domain\Listener;

use Cidadania\Business\Command\PaymentParcels\CreateCommand;
use Cidadania\Business\Command\PaymentParcels\DeleteCommand;
use Cidadania\Domain\Event\PaymentEvent;
use Cidadania\Domain\Model\PaymentParcels;
use League\Tactician\CommandBus;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PaymentListener implements EventSubscriberInterface
{
    /**
     * @var CommandBus
     */
    protected $bus;

    /**
     * PaymentListener constructor.
     *
     * @param CommandBus $bus
     */
    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param PaymentEvent $event
     */
    public function onPaymentCreated(PaymentEvent $event)
    {
        // delete all the records existed
        $this->bus->handle(
            new DeleteCommand([
                PaymentParcels::PAYMENT => $event->getId()
            ])
        );

        $price = round($event->getTotalPrice() / $event->getParcels(), 2);

        $date = new \DateTimeImmutable();
        for ($i = 0; $i < $event->getParcels(); $i++) {
            if ($i > 0) {
                $date = $date->add(
                    new \DateInterval('P1M')
                );
            }

            $command = new CreateCommand([
                'payment' => $event->getId(),
                PaymentParcels::PARCEL_PRICE => $price,
                PaymentParcels::IS_PAID => false,
                PaymentParcels::PAYMENT_DATE => $date->format('Y-m-d'),
            ]);

            $this->bus->handle($command);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PaymentEvent::EVENT_NAME => [
                'onPaymentCreated',
                0,
            ],
        ];
    }
}
