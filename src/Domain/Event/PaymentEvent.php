<?php

namespace Cidadania\Domain\Event;

use Symfony\Component\EventDispatcher\Event;

class PaymentEvent extends Event
{
    const EVENT_NAME = 'payment.created';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $parcels;

    /**
     * @var float
     */
    protected $totalPrice;

    /**
     * @var string
     */
    protected $startPayment;

    /**
     * PaymentEvent constructor.
     *
     * @param int $id
     * @param int $parcels
     * @param float $totalPrice
     * @param string $startPayment
     */
    public function __construct(int $id, int $parcels, float $totalPrice, string $startPayment)
    {
        $this->id = $id;
        $this->parcels = $parcels;
        $this->totalPrice = $totalPrice;
        $this->startPayment = $startPayment;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getParcels(): int
    {
        return $this->parcels;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    /**
     * @return string
     */
    public function getStartPayment(): string
    {
        return $this->startPayment;
    }
}
