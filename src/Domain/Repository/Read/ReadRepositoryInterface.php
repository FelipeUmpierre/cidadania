<?php

namespace Cidadania\Domain\Repository\Read;

use Cidadania\Domain\Model\Entity;

interface ReadRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return Entity
     */
    public function find(int $id): Entity;

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findBy(array $criteria = []): array;
}
