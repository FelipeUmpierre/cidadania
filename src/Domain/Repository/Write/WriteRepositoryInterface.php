<?php

namespace Cidadania\Domain\Repository\Write;

use Cidadania\Domain\Model\Entity;

interface WriteRepositoryInterface
{
    /**
     * @param Entity $entity
     *
     * @return int returns the id of the entity inserted on the database
     */
    public function save(Entity $entity): int;

    /**
     * @param int|array $id
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function delete($id): int;
}
