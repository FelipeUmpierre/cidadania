<?php

namespace Cidadania\Domain\Repository\Write;

interface TableNameInterface
{
    /**
     * @return string
     */
    public function getTableName(): string;
}
