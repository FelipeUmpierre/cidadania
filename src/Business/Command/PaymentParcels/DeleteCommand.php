<?php

namespace Cidadania\Business\Command\PaymentParcels;

use Cidadania\Business\Command\Entity\DeleteEntityAbstract;

final class DeleteCommand extends DeleteEntityAbstract
{
    /**
     * @var array
     */
    public $id;

    /**
     * DeleteCommand constructor.
     *
     * @param array $id
     */
    public function __construct(array $id)
    {
        $this->id = $id;
    }
}
