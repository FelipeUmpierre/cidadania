<?php

namespace Cidadania\Business\Command\Entity;

abstract class DeleteEntityAbstract
{
    /**
     * @var int
     */
    public $id;

    /**
     * DeleteEntityAbstract constructor.
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
