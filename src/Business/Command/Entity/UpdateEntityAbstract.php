<?php

namespace Cidadania\Business\Command\Entity;

abstract class UpdateEntityAbstract
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var array
     */
    public $payload;

    /**
     * UpdateEntityAbstract constructor.
     *
     * @param int $id
     * @param array $payload
     */
    public function __construct(int $id, array $payload)
    {
        $this->id = $id;
        $this->payload = $payload;
    }
}
