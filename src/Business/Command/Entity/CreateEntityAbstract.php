<?php

namespace Cidadania\Business\Command\Entity;

abstract class CreateEntityAbstract
{
    /**
     * @var array
     */
    public $payload;

    /**
     * CreateEntityAbstract constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }
}
