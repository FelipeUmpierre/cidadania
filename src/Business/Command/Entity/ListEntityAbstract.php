<?php

namespace Cidadania\Business\Command\Entity;

abstract class ListEntityAbstract
{
    /**
     * @var array
     */
    public $criteria;

    /**
     * ListEntityAbstract constructor.
     *
     * @param array $criteria
     */
    public function __construct(array $criteria = [])
    {
        $this->criteria = $criteria;
    }
}
