<?php

namespace Cidadania\Business\Command\Entity;

abstract class ShowEntityAbstract
{
    /**
     * @var int
     */
    public $id;

    /**
     * ShowEntityAbstract constructor.
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
