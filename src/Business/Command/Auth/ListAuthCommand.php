<?php

namespace Cidadania\Business\Command\Auth;

class ListAuthCommand
{
    /**
     * @var int|null
     */
    public $id;

    /**
     * ListAuthCommand constructor.
     *
     * @param int|null $id
     */
    public function __construct(?int $id)
    {
        $this->id = $id;
    }
}