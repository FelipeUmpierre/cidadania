<?php

namespace Cidadania\Business\Command\Auth;

class WriteAuthCommand implements \JsonSerializable
{
    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * ListAuthCommand constructor.
     *
     * @param int|null $id
     * @param string $username
     * @param string $password
     */
    public function __construct(?int $id, string $username, string $password)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @param array $elem
     *
     * @return WriteAuthCommand
     */
    public static function create(array $elem): WriteAuthCommand
    {
        return new self(
            $elem['id'] ?? null,
            $elem['username'],
            $elem['password']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): array
    {
        return array_filter([
            'id' => $this->id,
            'username' => $this->username,
            'password' => $this->password,
        ], function ($v) {
            return $v !== null;
        });
    }
}