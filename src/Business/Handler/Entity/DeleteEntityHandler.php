<?php

namespace Cidadania\Business\Handler\Entity;

use Cidadania\Business\Command\Entity\DeleteEntityAbstract;
use Cidadania\Domain\Repository\Write\WriteRepositoryInterface;

class DeleteEntityHandler
{
    private $repository;

    public function __construct(WriteRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(DeleteEntityAbstract $command): int
    {
        return $this->repository->delete($command->id);
    }
}
