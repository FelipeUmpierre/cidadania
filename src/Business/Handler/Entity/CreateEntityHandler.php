<?php

namespace Cidadania\Business\Handler\Entity;

use Cidadania\Domain\Model\Entity;
use Cidadania\Application\Contract\Form;
use Symfony\Component\Form\FormInterface;
use Cidadania\Business\Command\Entity\CreateEntityAbstract;
use Cidadania\Domain\Repository\Write\WriteRepositoryInterface;

class CreateEntityHandler
{
    /**
     * @var WriteRepositoryInterface
     */
    private $repository;

    /**
     * @var FormInterface
     */
    private $form;

    /**
     * CreateEntityHandler constructor.
     *
     * @param WriteRepositoryInterface $repository
     * @param Form $form
     */
    public function __construct(WriteRepositoryInterface $repository, Form $form)
    {
        $this->repository = $repository;
        $this->form = $form;
    }

    public function handle(CreateEntityAbstract $command): Entity
    {
        /** @var Entity $entity */
        $entity = $this->form->submit($command->payload);
        $entity->setId(
            $this->repository->save(
                $entity
            )
        );

        return $entity;
    }
}
