<?php

namespace Cidadania\Business\Handler\Entity;

use Cidadania\Business\Command\Entity\ListEntityAbstract;
use Cidadania\Domain\Repository\Read\ReadRepositoryInterface;

class ListEntityHandler
{
    /**
     * @var ReadRepositoryInterface
     */
    private $repository;

    /**
     * ListEntityHandler constructor.
     *
     * @param ReadRepositoryInterface $repository
     */
    public function __construct(ReadRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ListEntityAbstract $command
     *
     * @return array
     */
    public function handle(ListEntityAbstract $command): array
    {
        return $this->repository->findBy($command->criteria);
    }
}
