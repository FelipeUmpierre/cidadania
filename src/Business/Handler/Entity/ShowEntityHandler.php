<?php

namespace Cidadania\Business\Handler\Entity;

use Cidadania\Domain\Model\Entity;
use Cidadania\Business\Command\Entity\ShowEntityAbstract;
use Cidadania\Domain\Repository\Read\ReadRepositoryInterface;

class ShowEntityHandler
{
    /**
     * @var ReadRepositoryInterface
     */
    private $repository;

    /**
     * ShowEntityHandler constructor.
     *
     * @param ReadRepositoryInterface $repository
     */
    public function __construct(ReadRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ShowEntityAbstract $command
     *
     * @return Entity
     */
    public function handle(ShowEntityAbstract $command): Entity
    {
        return $this->repository->find($command->id);
    }
}
