<?php

namespace Cidadania\Business\Handler\Auth;

use Cidadania\Business\Command\Auth\ListAuthCommand;
use Cidadania\Domain\Repository\Read\AuthRepositoryInterface;

class ListAuthHandler
{
    /**
     * @var AuthRepositoryInterface
     */
    protected $repository;

    /**
     * ListAuthHandler constructor.
     *
     * @param AuthRepositoryInterface $repository
     */
    public function __construct(AuthRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ListAuthCommand $command
     *
     * @return \Cidadania\Domain\Model\Auth|\Collections\Vector
     */
    public function handle(ListAuthCommand $command)
    {
        if ($command->id !== null) {
            return $this->repository->find($command->id);
        }

        return $this->repository->findBy();
    }
}