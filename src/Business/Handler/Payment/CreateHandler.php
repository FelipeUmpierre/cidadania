<?php

namespace Cidadania\Business\Handler\Payment;

use Cidadania\Application\Contract\Form;
use Cidadania\Business\Command\Entity\CreateEntityAbstract;
use Cidadania\Business\Handler\Entity\CreateEntityHandler;
use Cidadania\Domain\Event\PaymentEvent;
use Cidadania\Domain\Model\Entity;
use Cidadania\Domain\Model\Payment;
use Cidadania\Domain\Repository\Write\WriteRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateHandler extends CreateEntityHandler
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * CreateHandler constructor.
     *
     * @param WriteRepositoryInterface $repository
     * @param Form $form
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(WriteRepositoryInterface $repository, Form $form, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($repository, $form);

        $this->dispatcher = $dispatcher;
    }

    /**
     * @param CreateEntityAbstract $command
     *
     * @return Entity
     */
    public function handle(CreateEntityAbstract $command): Entity
    {
        /** @var Payment $entity */
        $entity = parent::handle($command);

        $event = new PaymentEvent(
            $entity->getId(),
            $entity->getParcels(),
            $entity->getTotalPrice(),
            $entity->getStartPayment()
        );

        $this->dispatcher->dispatch(PaymentEvent::EVENT_NAME, $event);

        return $entity;
    }
}
