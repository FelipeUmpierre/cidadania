SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema cidadania
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema cidadania
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cidadania` DEFAULT CHARACTER SET utf8 ;
USE `cidadania` ;

-- -----------------------------------------------------
-- Table `service`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `service` ;

CREATE TABLE IF NOT EXISTS `service` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `price` DOUBLE(10,2) NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `assistence`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `assistence` ;

CREATE TABLE IF NOT EXISTS `assistence` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `partner`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `partner` ;

CREATE TABLE IF NOT EXISTS `partner` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fk_service` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL COMMENT '\n',
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_partner_service1_idx` (`fk_service` ASC),
  CONSTRAINT `fk_partner_service1`
  FOREIGN KEY (`fk_service`)
  REFERENCES `service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contact` ;

CREATE TABLE IF NOT EXISTS `contact` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fk_partner` INT NULL,
  `fk_assistence` INT NULL,
  `code` VARCHAR(45) NULL COMMENT 'backend generated code (001/2017) and so on by record + year',
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `birthday` VARCHAR(45) NOT NULL,
  `address` VARCHAR(225) NOT NULL,
  `passport` VARCHAR(45) NOT NULL,
  `schooling` VARCHAR(45) NOT NULL,
  `uk_entrance_date` DATE NOT NULL,
  `father_name` VARCHAR(45) NOT NULL,
  `mother_name` VARCHAR(45) NOT NULL,
  `profession` VARCHAR(45) NOT NULL,
  `observation` TEXT NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `is_client` BOOL NOT NULL DEFAULT FALSE,
  `is_done` BOOL NOT NULL DEFAULT FALSE COMMENT 'Check if the contact process is done',
  `contacted_at` DATE NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_contact_partner1_idx` (`fk_partner` ASC),
  CONSTRAINT `fk_contact_partner1`
  FOREIGN KEY (`fk_partner`)
  REFERENCES `partner` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  INDEX `fk_contact_assistence1_idx` (`fk_assistence` ASC),
  CONSTRAINT `fk_contact_assistence1`
  FOREIGN KEY (`fk_assistence`)
  REFERENCES `assistence` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `contact_upload`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contact_upload` ;

CREATE TABLE IF NOT EXISTS `contact_upload` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fk_contact` INT NOT NULL,
  `file` VARCHAR(255) NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_contact_upload_contact_idx` (`fk_contact` ASC),
  CONSTRAINT `fk_contact_upload_contact`
  FOREIGN KEY (`fk_contact`)
  REFERENCES `contact` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `currency`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `currency` ;

CREATE TABLE IF NOT EXISTS `currency` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `symbol` VARCHAR(10) NOT NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `payment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `payment` ;

CREATE TABLE IF NOT EXISTS `payment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fk_currency` INT NOT NULL,
  `fk_contact` INT NOT NULL,
  `parcels` INT NOT NULL,
  `price` DOUBLE(10,2) NULL,
  `partner_price` DOUBLE(10,2) NULL,
  `total_price` DOUBLE(10,2) NULL,
  `start_payment` DATE NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_contact1_idx` (`fk_contact` ASC),
  INDEX `fk_payment_currency1_idx` (`fk_currency` ASC),
  CONSTRAINT `fk_payment_contact1`
  FOREIGN KEY (`fk_contact`)
  REFERENCES `contact` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_currency1`
  FOREIGN KEY (`fk_currency`)
  REFERENCES `currency` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `payment_parcel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `payment_parcel` ;

CREATE TABLE IF NOT EXISTS `payment_parcel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fk_payment` INT NOT NULL,
  `parcel_price` DOUBLE(10,2) NULL,
  `is_paid` TINYINT(1) NULL DEFAULT 0,
  `payment_date` DATE NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_parcel_payment1_idx` (`fk_payment` ASC),
  CONSTRAINT `fk_payment_parcel_payment1`
  FOREIGN KEY (`fk_payment`)
  REFERENCES `payment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `auth`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `auth` ;

CREATE TABLE IF NOT EXISTS `auth` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `roles` VARCHAR(255) NOT NULL,
  `deleted_at` DATETIME NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- INSERTS
-- -----------------------------------------------------
INSERT INTO currency (`name`, `symbol`) VALUES ('British Pounds', 'GBP');
INSERT INTO auth (username, password, roles) VALUES ('admin', '$2y$13$VDu2iDseA6MFXp2O5bgo3.kcJKoy0CDq.lD/rRvy5hMVSQvpXb2vG', 'ROLES_ADMIN');
